export const demo = {
    comment:
      "Neutron Monitor database query - [OULU, DOMC, DOMB NM detectors]- http://cosmicrays.oulu.fi",
    station: "OULU NEUTRON MONITOR",
    info: [
      "Auto resolution applied.",
      "Units for date column: fractional years",
      "Columns: #1, #2 -date and time, #3 - fractional date, #4, #5 - uncorrected and corrected (for pressure and efficiency ) count rates [counts/min], #6 - barometric pressure [mbar]"
    ],
    start: "1964-04-01T00:00:00Z",
    end: " 2021-08-04T23:30:00Z",
    effectivenessCorrections: [
      { corrFactor: 1, from: " 1964-01-01T00:00:00Z" },
      { corrFactor: 1, from: " 1964-01-01T00:00:00Z" },
      { corrFactor: 1.00674, from: " 1985-10-01T00:00:00Z" },
      { corrFactor: 1.00674, from: " 1985-10-01T00:00:00Z" },
      { corrFactor: 1.01147, from: " 1995-01-01T00:00:00Z" },
      { corrFactor: 1.01147, from: " 1995-01-01T00:00:00Z" },
      { corrFactor: 1.00914, from: " 2000-01-01T00:00:00Z" },
      { corrFactor: 1.00914, from: " 2000-01-01T00:00:00Z" },
      { corrFactor: 1.00813, from: " 2003-06-01T00:00:00Z" },
      { corrFactor: 1.00813, from: " 2003-06-01T00:00:00Z" },
      { corrFactor: 1.0029, from: " 2008-08-01T00:00:00Z" },
      { corrFactor: 1.0029, from: " 2008-08-01T00:00:00Z" },
      { corrFactor: 1.0019, from: " 2009-11-01T00:00:00Z" },
      { corrFactor: 1.0019, from: " 2009-11-01T00:00:00Z" }
    ],
    pressureCorrections: [
      { corrFactor: -0.0074, from: " 1964-01-01T00:00:00Z" },
      { corrFactor: -0.0074, from: " 1964-01-01T00:00:00Z" }
    ],
    data: [
      {
        time: "1964-04-01T00:00:00Z",
        fractionalDate: 1964.2486339,
        uncorr: 5963,
        corr: 6414,
        pressure: 1009.98
      },
      {
        time: "1964-05-01T00:00:00Z",
        fractionalDate: 1964.3306011,
        uncorr: 5945,
        corr: 6424,
        pressure: 1010.69
      },
      {
        time: "1964-06-01T00:00:00Z",
        fractionalDate: 1964.4153005,
        uncorr: 6008,
        corr: 6427,
        pressure: 1009.32
      },
      {
        time: "1964-07-01T00:00:00Z",
        fractionalDate: 1964.4972678,
        uncorr: 6059,
        corr: 6461,
        pressure: 1009.03
      },
      {
        time: "1964-08-01T00:00:00Z",
        fractionalDate: 1964.5819672,
        uncorr: 6112,
        corr: 6464,
        pressure: 1007.79
      },
      {
        time: "1964-09-01T00:00:00Z",
        fractionalDate: 1964.6666667,
        uncorr: 6299,
        corr: 6491,
        pressure: 1004.47
      },
      {
        time: "1964-10-01T00:00:00Z",
        fractionalDate: 1964.7486339,
        uncorr: 5865,
        corr: 6474,
        pressure: 1013.57
      },
      {
        time: "1964-11-01T00:00:00Z",
        fractionalDate: 1964.8333333,
        uncorr: 6389,
        corr: 6468,
        pressure: 1002.23
      },
      {
        time: "1964-12-01T00:00:00Z",
        fractionalDate: 1964.9153005,
        uncorr: 6359,
        corr: 6512,
        pressure: 1003.72
      },
      {
        time: "1965-01-01T00:00:00Z",
        fractionalDate: 1965.0,
        uncorr: 6259,
        corr: 6506,
        pressure: 1006.38
      },
      {
        time: "1965-02-01T00:00:00Z",
        fractionalDate: 1965.0849315,
        uncorr: 6113,
        corr: 6467,
        pressure: 1008.14
      },
      {
        time: "1965-03-01T00:00:00Z",
        fractionalDate: 1965.1616438,
        uncorr: 6211,
        corr: 6532,
        pressure: 1007.13
      },
      {
        time: "1965-04-01T00:00:00Z",
        fractionalDate: 1965.2465753,
        uncorr: 5876,
        corr: 6593,
        pressure: 1015.7
      },
      {
        time: "1965-05-01T00:00:00Z",
        fractionalDate: 1965.3287671,
        uncorr: 6015,
        corr: 6620,
        pressure: 1013.19
      },
      {
        time: "1965-06-01T00:00:00Z",
        fractionalDate: 1965.4136986,
        uncorr: 6142,
        corr: 6513,
        pressure: 1008.13
      },
      {
        time: "1965-07-01T00:00:00Z",
        fractionalDate: 1965.4958904,
        uncorr: 6303,
        corr: 6473,
        pressure: 1003.98
      },
      {
        time: "1965-08-01T00:00:00Z",
        fractionalDate: 1965.5808219,
        uncorr: 6118,
        corr: 6454,
        pressure: 1007.47
      },
      {
        time: "1965-09-01T00:00:00Z",
        fractionalDate: 1965.6657534,
        uncorr: 6056,
        corr: 6471,
        pressure: 1009.18
      },
      {
        time: "1965-10-01T00:00:00Z",
        fractionalDate: 1965.7479452,
        uncorr: 6157,
        corr: 6509,
        pressure: 1008.15
      },
      {
        time: "1965-11-01T00:00:00Z",
        fractionalDate: 1965.8328767,
        uncorr: 6101,
        corr: 6559,
        pressure: 1010.3
      },
      {
        time: "1965-12-01T00:00:00Z",
        fractionalDate: 1965.9150685,
        uncorr: 6535,
        corr: 6508,
        pressure: 999.9
      },
      {
        time: "1966-01-01T00:00:00Z",
        fractionalDate: 1966.0,
        uncorr: 5563,
        corr: 6424,
        pressure: 1020.26
      },
      {
        time: "1966-02-01T00:00:00Z",
        fractionalDate: 1966.0849315,
        uncorr: 5941,
        corr: 6376,
        pressure: 1009.96
      },
      {
        time: "1966-03-01T00:00:00Z",
        fractionalDate: 1966.1616438,
        uncorr: 6156,
        corr: 6411,
        pressure: 1005.74
      },
      {
        time: "1966-04-01T00:00:00Z",
        fractionalDate: 1966.2465753,
        uncorr: 5673,
        corr: 6397,
        pressure: 1016.33
      },
      {
        time: "1966-05-01T00:00:00Z",
        fractionalDate: 1966.3287671,
        uncorr: 5877,
        corr: 6435,
        pressure: 1012.48
      },
      {
        time: "1966-06-01T00:00:00Z",
        fractionalDate: 1966.4136986,
        uncorr: 5813,
        corr: 6346,
        pressure: 1012.09
      },
      {
        time: "1966-07-01T00:00:00Z",
        fractionalDate: 1966.4958904,
        uncorr: 6055,
        corr: 6304,
        pressure: 1005.57
      },
      {
        time: "1966-08-01T00:00:00Z",
        fractionalDate: 1966.5808219,
        uncorr: 5828,
        corr: 6277,
        pressure: 1010.25
      },
      {
        time: "1966-09-01T00:00:00Z",
        fractionalDate: 1966.6657534,
        uncorr: 6099,
        corr: 6041,
        pressure: 998.9
      },
      {
        time: "1966-10-01T00:00:00Z",
        fractionalDate: 1966.7479452,
        uncorr: 5778,
        corr: 6162,
        pressure: 1009.16
      },
      {
        time: "1966-11-01T00:00:00Z",
        fractionalDate: 1966.8328767,
        uncorr: 5703,
        corr: 6220,
        pressure: 1012.0
      },
      {
        time: "1966-12-01T00:00:00Z",
        fractionalDate: 1966.9150685,
        uncorr: 5636,
        corr: 6155,
        pressure: 1012.39
      },
      {
        time: "1967-01-01T00:00:00Z",
        fractionalDate: 1967.0,
        uncorr: 5529,
        corr: 6022,
        pressure: 1011.97
      },
      {
        time: "1967-02-01T00:00:00Z",
        fractionalDate: 1967.0849315,
        uncorr: 5630,
        corr: 5962,
        pressure: 1008.49
      },
      {
        time: "1967-03-01T00:00:00Z",
        fractionalDate: 1967.1616438,
        uncorr: 6413,
        corr: 6075,
        pressure: 993.06
      },
      {
        time: "1967-04-01T00:00:00Z",
        fractionalDate: 1967.2465753,
        uncorr: 5833,
        corr: 6152,
        pressure: 1008.16
      },
      {
        time: "1967-05-01T00:00:00Z",
        fractionalDate: 1967.3287671,
        uncorr: 5401,
        corr: 6041,
        pressure: 1015.55
      },
      {
        time: "1967-06-01T00:00:00Z",
        fractionalDate: 1967.4136986,
        uncorr: 5622,
        corr: 5996,
        pressure: 1009.08
      },
      {
        time: "1967-07-01T00:00:00Z",
        fractionalDate: 1967.4958904,
        uncorr: 5607,
        corr: 6048,
        pressure: 1010.35
      },
      {
        time: "1967-08-01T00:00:00Z",
        fractionalDate: 1967.5808219,
        uncorr: 5593,
        corr: 5982,
        pressure: 1009.18
      },
      {
        time: "1967-09-01T00:00:00Z",
        fractionalDate: 1967.6657534,
        uncorr: 5534,
        corr: 6038,
        pressure: 1011.99
      },
      {
        time: "1967-10-01T00:00:00Z",
        fractionalDate: 1967.7479452,
        uncorr: 6234,
        corr: 6069,
        pressure: 996.66
      },
      {
        time: "1967-11-01T00:00:00Z",
        fractionalDate: 1967.8328767,
        uncorr: 5648,
        corr: 6010,
        pressure: 1008.75
      },
      {
        time: "1967-12-01T00:00:00Z",
        fractionalDate: 1967.9150685,
        uncorr: 5955,
        corr: 6004,
        pressure: 1001.52
      },
      {
        time: "1968-01-01T00:00:00Z",
        fractionalDate: 1968.0,
        uncorr: 5728,
        corr: 5953,
        pressure: 1005.42
      },
      {
        time: "1968-02-01T00:00:00Z",
        fractionalDate: 1968.0846995,
        uncorr: 5609,
        corr: 5879,
        pressure: 1007.28
      },
      {
        time: "1968-03-01T00:00:00Z",
        fractionalDate: 1968.1639344,
        uncorr: 6225,
        corr: 5913,
        pressure: 993.75
      },
      {
        time: "1968-04-01T00:00:00Z",
        fractionalDate: 1968.2486339,
        uncorr: 5670,
        corr: 6049,
        pressure: 1009.3
      },
      {
        time: "1968-05-01T00:00:00Z",
        fractionalDate: 1968.3306011,
        uncorr: 5405,
        corr: 5984,
        pressure: 1014.29
      },
      {
        time: "1968-06-01T00:00:00Z",
        fractionalDate: 1968.4153005,
        uncorr: 5539,
        corr: 5905,
        pressure: 1009.0
      },
      {
        time: "1968-07-01T00:00:00Z",
        fractionalDate: 1968.4972678,
        uncorr: 5375,
        corr: 5925,
        pressure: 1013.21
      },
      {
        time: "1968-08-01T00:00:00Z",
        fractionalDate: 1968.5819672,
        uncorr: 5462,
        corr: 5943,
        pressure: 1011.84
      },
      {
        time: "1968-09-01T00:00:00Z",
        fractionalDate: 1968.6666667,
        uncorr: 5246,
        corr: 5894,
        pressure: 1016.19
      },
      {
        time: "1968-10-01T00:00:00Z",
        fractionalDate: 1968.7486339,
        uncorr: 5554,
        corr: 5801,
        pressure: 1006.26
      },
      {
        time: "1968-11-01T00:00:00Z",
        fractionalDate: 1968.8333333,
        uncorr: 4958,
        corr: 5623,
        pressure: 1017.36
      },
      {
        time: "1968-12-01T00:00:00Z",
        fractionalDate: 1968.9153005,
        uncorr: 5328,
        corr: 5655,
        pressure: 1008.21
      },
      {
        time: "1969-01-01T00:00:00Z",
        fractionalDate: 1969.0,
        uncorr: 4988,
        corr: 5771,
        pressure: 1020.13
      },
      {
        time: "1969-02-01T00:00:00Z",
        fractionalDate: 1969.0849315,
        uncorr: 4905,
        corr: 5730,
        pressure: 1021.4
      },
      {
        time: "1969-03-01T00:00:00Z",
        fractionalDate: 1969.1616438,
        uncorr: 5148,
        corr: 5744,
        pressure: 1016.03
      },
      {
        time: "1969-04-01T00:00:00Z",
        fractionalDate: 1969.2465753,
        uncorr: 5399,
        corr: 5829,
        pressure: 1010.78
      },
      {
        time: "1969-05-01T00:00:00Z",
        fractionalDate: 1969.3287671,
        uncorr: 5126,
        corr: 5687,
        pressure: 1014.22
      },
      {
        time: "1969-06-01T00:00:00Z",
        fractionalDate: 1969.4136986,
        uncorr: 5125,
        corr: 5628,
        pressure: 1012.73
      },
      {
        time: "1969-07-01T00:00:00Z",
        fractionalDate: 1969.4958904,
        uncorr: 5229,
        corr: 5690,
        pressure: 1011.61
      },
      {
        time: "1969-08-01T00:00:00Z",
        fractionalDate: 1969.5808219,
        uncorr: 5159,
        corr: 5785,
        pressure: 1015.88
      },
      {
        time: "1969-09-01T00:00:00Z",
        fractionalDate: 1969.6657534,
        uncorr: 5539,
        corr: 5833,
        pressure: 1007.24
      },
      {
        time: "1969-10-01T00:00:00Z",
        fractionalDate: 1969.7479452,
        uncorr: 5666,
        corr: 5860,
        pressure: 1004.99
      },
      {
        time: "1969-11-01T00:00:00Z",
        fractionalDate: 1969.8328767,
        uncorr: 6150,
        corr: 5850,
        pressure: 993.6
      },
      {
        time: "1969-12-01T00:00:00Z",
        fractionalDate: 1969.9150685,
        uncorr: 5112,
        corr: 5864,
        pressure: 1019.0
      },
      {
        time: "1970-01-01T00:00:00Z",
        fractionalDate: 1970.0,
        uncorr: 5287,
        corr: 5792,
        pressure: 1012.95
      },
      {
        time: "1970-02-01T00:00:00Z",
        fractionalDate: 1970.0849315,
        uncorr: 5667,
        corr: 5790,
        pressure: 1003.68
      },
      {
        time: "1970-03-01T00:00:00Z",
        fractionalDate: 1970.1616438,
        uncorr: 5521,
        corr: 5795,
        pressure: 1007.32
      },
      {
        time: "1970-04-01T00:00:00Z",
        fractionalDate: 1970.2465753,
        uncorr: 5493,
        corr: 5803,
        pressure: 1007.58
      },
      {
        time: "1970-05-01T00:00:00Z",
        fractionalDate: 1970.3287671,
        uncorr: 5264,
        corr: 5841,
        pressure: 1014.47
      },
      {
        time: "1970-06-01T00:00:00Z",
        fractionalDate: 1970.4136986,
        uncorr: 5069,
        corr: 5699,
        pressure: 1015.98
      },
      {
        time: "1970-07-01T00:00:00Z",
        fractionalDate: 1970.4958904,
        uncorr: 5527,
        corr: 5690,
        pressure: 1004.15
      },
      {
        time: "1970-08-01T00:00:00Z",
        fractionalDate: 1970.5808219,
        uncorr: 5266,
        corr: 5781,
        pressure: 1012.98
      },
      {
        time: "1970-09-01T00:00:00Z",
        fractionalDate: 1970.6657534,
        uncorr: 5601,
        corr: 5870,
        pressure: 1006.66
      },
      {
        time: "1970-10-01T00:00:00Z",
        fractionalDate: 1970.7479452,
        uncorr: 5657,
        corr: 5910,
        pressure: 1006.5
      },
      {
        time: "1970-11-01T00:00:00Z",
        fractionalDate: 1970.8328767,
        uncorr: 5511,
        corr: 5794,
        pressure: 1007.16
      },
      {
        time: "1970-12-01T00:00:00Z",
        fractionalDate: 1970.9150685,
        uncorr: 5608,
        corr: 5939,
        pressure: 1008.1
      },
      {
        time: "1971-01-01T00:00:00Z",
        fractionalDate: 1971.0,
        uncorr: 5821,
        corr: 5910,
        pressure: 1002.52
      },
      {
        time: "1971-02-01T00:00:00Z",
        fractionalDate: 1971.0849315,
        uncorr: 5522,
        corr: 6004,
        pressure: 1011.9
      },
      {
        time: "1971-03-01T00:00:00Z",
        fractionalDate: 1971.1616438,
        uncorr: 5462,
        corr: 5956,
        pressure: 1012.65
      },
      {
        time: "1971-04-01T00:00:00Z",
        fractionalDate: 1971.2465753,
        uncorr: 5526,
        corr: 6089,
        pressure: 1013.39
      },
      {
        time: "1971-05-01T00:00:00Z",
        fractionalDate: 1971.3287671,
        uncorr: 5487,
        corr: 6132,
        pressure: 1015.18
      },
      {
        time: "1971-06-01T00:00:00Z",
        fractionalDate: 1971.4136986,
        uncorr: 5865,
        corr: 6254,
        pressure: 1008.87
      },
      {
        time: "1971-07-01T00:00:00Z",
        fractionalDate: 1971.4958904,
        uncorr: 5738,
        corr: 6266,
        pressure: 1012.11
      },
      {
        time: "1971-08-01T00:00:00Z",
        fractionalDate: 1971.5808219,
        uncorr: 6052,
        corr: 6320,
        pressure: 1006.05
      },
      {
        time: "1971-09-01T00:00:00Z",
        fractionalDate: 1971.6657534,
        uncorr: 5765,
        corr: 6323,
        pressure: 1012.87
      },
      {
        time: "1971-10-01T00:00:00Z",
        fractionalDate: 1971.7479452,
        uncorr: 6161,
        corr: 6387,
        pressure: 1005.51
      },
      {
        time: "1971-11-01T00:00:00Z",
        fractionalDate: 1971.8328767,
        uncorr: 6488,
        corr: 6348,
        pressure: 997.78
      },
      {
        time: "1971-12-01T00:00:00Z",
        fractionalDate: 1971.9150685,
        uncorr: 6029,
        corr: 6295,
        pressure: 1006.21
      },
      {
        time: "1972-01-01T00:00:00Z",
        fractionalDate: 1972.0,
        uncorr: 5091,
        corr: 6227,
        pressure: 1027.55
      },
      {
        time: "1972-02-01T00:00:00Z",
        fractionalDate: 1972.0846995,
        uncorr: 5364,
        corr: 6130,
        pressure: 1018.5
      },
      {
        time: "1972-03-01T00:00:00Z",
        fractionalDate: 1972.1639344,
        uncorr: 5911,
        corr: 6391,
        pressure: 1012.05
      },
      {
        time: "1972-04-01T00:00:00Z",
        fractionalDate: 1972.2486339,
        uncorr: 6152,
        corr: 6445,
        pressure: 1006.48
      },
      {
        time: "1972-05-01T00:00:00Z",
        fractionalDate: 1972.3306011,
        uncorr: 5737,
        corr: 6410,
        pressure: 1015.41
      },
      {
        time: "1972-06-01T00:00:00Z",
        fractionalDate: 1972.4153005,
        uncorr: 5711,
        corr: 6270,
        pressure: 1012.7
      },
      {
        time: "1972-07-01T00:00:00Z",
        fractionalDate: 1972.4972678,
        uncorr: 5746,
        corr: 6357,
        pressure: 1013.78
      },
      {
        time: "1972-08-01T00:00:00Z",
        fractionalDate: 1972.5819672,
        uncorr: 5560,
        corr: 6093,
        pressure: 1012.53
      },
      {
        time: "1972-09-01T00:00:00Z",
        fractionalDate: 1972.6666667,
        uncorr: 5810,
        corr: 6348,
        pressure: 1012.14
      },
      {
        time: "1972-10-01T00:00:00Z",
        fractionalDate: 1972.7486339,
        uncorr: 5934,
        corr: 6354,
        pressure: 1009.63
      },
      {
        time: "1972-11-01T00:00:00Z",
        fractionalDate: 1972.8333333,
        uncorr: 6292,
        corr: 6149,
        pressure: 997.34
      },
      {
        time: "1972-12-01T00:00:00Z",
        fractionalDate: 1972.9153005,
        uncorr: 5971,
        corr: 6355,
        pressure: 1009.14
      },
      {
        time: "1973-01-01T00:00:00Z",
        fractionalDate: 1973.0,
        uncorr: 5758,
        corr: 6366,
        pressure: 1014.06
      },
      {
        time: "1973-02-01T00:00:00Z",
        fractionalDate: 1973.0849315,
        uncorr: 6442,
        corr: 6334,
        pressure: 998.44
      },
      {
        time: "1973-03-01T00:00:00Z",
        fractionalDate: 1973.1616438,
        uncorr: 5893,
        corr: 6308,
        pressure: 1009.57
      },
      {
        time: "1973-04-01T00:00:00Z",
        fractionalDate: 1973.2465753,
        uncorr: 5948,
        corr: 6198,
        pressure: 1006.0
      },
      {
        time: "1973-05-01T00:00:00Z",
        fractionalDate: 1973.3287671,
        uncorr: 5575,
        corr: 6126,
        pressure: 1013.06
      },
      {
        time: "1973-06-01T00:00:00Z",
        fractionalDate: 1973.4136986,
        uncorr: 5762,
        corr: 6263,
        pressure: 1011.67
      },
      {
        time: "1973-07-01T00:00:00Z",
        fractionalDate: 1973.4958904,
        uncorr: 5794,
        corr: 6314,
        pressure: 1011.82
      },
      {
        time: "1973-08-01T00:00:00Z",
        fractionalDate: 1973.5808219,
        uncorr: 5969,
        corr: 6361,
        pressure: 1008.81
      },
      {
        time: "1973-09-01T00:00:00Z",
        fractionalDate: 1973.6657534,
        uncorr: 6018,
        corr: 6472,
        pressure: 1010.17
      },
      {
        time: "1973-10-01T00:00:00Z",
        fractionalDate: 1973.7479452,
        uncorr: 6154,
        corr: 6447,
        pressure: 1006.66
      },
      {
        time: "1973-11-01T00:00:00Z",
        fractionalDate: 1973.8328767,
        uncorr: 6755,
        corr: 6476,
        pressure: 994.88
      },
      {
        time: "1973-12-01T00:00:00Z",
        fractionalDate: 1973.9150685,
        uncorr: 6616,
        corr: 6475,
        pressure: 997.66
      },
      {
        time: "1974-01-01T00:00:00Z",
        fractionalDate: 1974.0,
        uncorr: 5925,
        corr: 6453,
        pressure: 1012.26
      },
      {
        time: "1974-02-01T00:00:00Z",
        fractionalDate: 1974.0849315,
        uncorr: 6290,
        corr: 6480,
        pressure: 1004.81
      },
      {
        time: "1974-03-01T00:00:00Z",
        fractionalDate: 1974.1616438,
        uncorr: 5388,
        corr: 6417,
        pressure: 1024.43
      },
      {
        time: "1974-04-01T00:00:00Z",
        fractionalDate: 1974.2465753,
        uncorr: 5781,
        corr: 6383,
        pressure: 1013.61
      },
      {
        time: "1974-05-01T00:00:00Z",
        fractionalDate: 1974.3287671,
        uncorr: 5644,
        corr: 6282,
        pressure: 1014.97
      },
      {
        time: "1974-06-01T00:00:00Z",
        fractionalDate: 1974.4136986,
        uncorr: 5826,
        corr: 6214,
        pressure: 1009.1
      },
      {
        time: "1974-07-01T00:00:00Z",
        fractionalDate: 1974.4958904,
        uncorr: 5997,
        corr: 6095,
        pressure: 1002.31
      },
      {
        time: "1974-08-01T00:00:00Z",
        fractionalDate: 1974.5808219,
        uncorr: 5814,
        corr: 6200,
        pressure: 1008.89
      },
      {
        time: "1974-09-01T00:00:00Z",
        fractionalDate: 1974.6657534,
        uncorr: 5898,
        corr: 6149,
        pressure: 1006.0
      },
      {
        time: "1974-10-01T00:00:00Z",
        fractionalDate: 1974.7479452,
        uncorr: 5649,
        corr: 6156,
        pressure: 1011.96
      },
      {
        time: "1974-11-01T00:00:00Z",
        fractionalDate: 1974.8328767,
        uncorr: 5782,
        corr: 6192,
        pressure: 1009.85
      },
      {
        time: "1974-12-01T00:00:00Z",
        fractionalDate: 1974.9150685,
        uncorr: 6480,
        corr: 6304,
        pressure: 996.49
      },
      {
        time: "1975-01-01T00:00:00Z",
        fractionalDate: 1975.0,
        uncorr: 6437,
        corr: 6319,
        pressure: 998.25
      },
      {
        time: "1975-02-01T00:00:00Z",
        fractionalDate: 1975.0849315,
        uncorr: 5910,
        corr: 6375,
        pressure: 1010.58
      },
      {
        time: "1975-03-01T00:00:00Z",
        fractionalDate: 1975.1616438,
        uncorr: 6088,
        corr: 6389,
        pressure: 1006.86
      },
      {
        time: "1975-04-01T00:00:00Z",
        fractionalDate: 1975.2465753,
        uncorr: 6047,
        corr: 6430,
        pressure: 1008.77
      },
      {
        time: "1975-05-01T00:00:00Z",
        fractionalDate: 1975.3287671,
        uncorr: 5842,
        corr: 6439,
        pressure: 1013.54
      },
      {
        time: "1975-06-01T00:00:00Z",
        fractionalDate: 1975.4136986,
        uncorr: 5947,
        corr: 6462,
        pressure: 1011.75
      },
      {
        time: "1975-07-01T00:00:00Z",
        fractionalDate: 1975.4958904,
        uncorr: 6039,
        corr: 6447,
        pressure: 1009.0
      },
      {
        time: "1975-08-01T00:00:00Z",
        fractionalDate: 1975.5808219,
        uncorr: 5835,
        corr: 6405,
        pressure: 1012.8
      },
      {
        time: "1975-09-01T00:00:00Z",
        fractionalDate: 1975.6657534,
        uncorr: 6288,
        corr: 6427,
        pressure: 1003.28
      },
      {
        time: "1975-10-01T00:00:00Z",
        fractionalDate: 1975.7479452,
        uncorr: 5787,
        corr: 6421,
        pressure: 1014.5
      },
      {
        time: "1975-11-01T00:00:00Z",
        fractionalDate: 1975.8328767,
        uncorr: 5926,
        corr: 6356,
        pressure: 1010.06
      },
      {
        time: "1975-12-01T00:00:00Z",
        fractionalDate: 1975.9150685,
        uncorr: 6938,
        corr: 6429,
        pressure: 990.25
      },
      {
        time: "1976-01-01T00:00:00Z",
        fractionalDate: 1976.0,
        uncorr: 6555,
        corr: 6417,
        pressure: 998.08
      },
      {
        time: "1976-02-01T00:00:00Z",
        fractionalDate: 1976.0846995,
        uncorr: 5708,
        corr: 6416,
        pressure: 1016.62
      },
      {
        time: "1976-03-01T00:00:00Z",
        fractionalDate: 1976.1639344,
        uncorr: 5673,
        corr: 6419,
        pressure: 1017.55
      },
      {
        time: "1976-04-01T00:00:00Z",
        fractionalDate: 1976.2486339,
        uncorr: 5988,
        corr: 6372,
        pressure: 1008.84
      },
      {
        time: "1976-05-01T00:00:00Z",
        fractionalDate: 1976.3306011,
        uncorr: 5704,
        corr: 6413,
        pressure: 1016.02
      },
      {
        time: "1976-06-01T00:00:00Z",
        fractionalDate: 1976.4153005,
        uncorr: 6037,
        corr: 6442,
        pressure: 1008.95
      },
      {
        time: "1976-07-01T00:00:00Z",
        fractionalDate: 1976.4972678,
        uncorr: 6021,
        corr: 6469,
        pressure: 1009.93
      },
      {
        time: "1976-08-01T00:00:00Z",
        fractionalDate: 1976.5819672,
        uncorr: 5799,
        corr: 6477,
        pressure: 1015.18
      },
      {
        time: "1976-09-01T00:00:00Z",
        fractionalDate: 1976.6666667,
        uncorr: 5986,
        corr: 6488,
        pressure: 1011.56
      },
      {
        time: "1976-10-01T00:00:00Z",
        fractionalDate: 1976.7486339,
        uncorr: 5582,
        corr: 6480,
        pressure: 1020.53
      },
      {
        time: "1976-11-01T00:00:00Z",
        fractionalDate: 1976.8333333,
        uncorr: 6102,
        corr: 6493,
        pressure: 1009.16
      },
      {
        time: "1976-12-01T00:00:00Z",
        fractionalDate: 1976.9153005,
        uncorr: 6030,
        corr: 6469,
        pressure: 1010.49
      },
      {
        time: "1977-01-01T00:00:00Z",
        fractionalDate: 1977.0,
        uncorr: 5887,
        corr: 6435,
        pressure: 1012.48
      },
      {
        time: "1977-02-01T00:00:00Z",
        fractionalDate: 1977.0849315,
        uncorr: 5949,
        corr: 6440,
        pressure: 1010.83
      },
      {
        time: "1977-03-01T00:00:00Z",
        fractionalDate: 1977.1616438,
        uncorr: 6024,
        corr: 6454,
        pressure: 1009.62
      },
      {
        time: "1977-04-01T00:00:00Z",
        fractionalDate: 1977.2465753,
        uncorr: 6291,
        corr: 6436,
        pressure: 1003.41
      },
      {
        time: "1977-05-01T00:00:00Z",
        fractionalDate: 1977.3287671,
        uncorr: 5749,
        corr: 6437,
        pressure: 1015.6
      },
      {
        time: "1977-06-01T00:00:00Z",
        fractionalDate: 1977.4136986,
        uncorr: 6051,
        corr: 6432,
        pressure: 1008.48
      },
      {
        time: "1977-07-01T00:00:00Z",
        fractionalDate: 1977.4958904,
        uncorr: 5982,
        corr: 6343,
        pressure: 1008.17
      },
      {
        time: "1977-08-01T00:00:00Z",
        fractionalDate: 1977.5808219,
        uncorr: 5696,
        corr: 6357,
        pressure: 1015.08
      },
      {
        time: "1977-09-01T00:00:00Z",
        fractionalDate: 1977.6657534,
        uncorr: 6009,
        corr: 6368,
        pressure: 1008.35
      },
      {
        time: "1977-10-01T00:00:00Z",
        fractionalDate: 1977.7479452,
        uncorr: 5941,
        corr: 6420,
        pressure: 1010.81
      },
      {
        time: "1977-11-01T00:00:00Z",
        fractionalDate: 1977.8328767,
        uncorr: 6731,
        corr: 6483,
        pressure: 995.73
      },
      {
        time: "1977-12-01T00:00:00Z",
        fractionalDate: 1977.9150685,
        uncorr: 5834,
        corr: 6453,
        pressure: 1014.47
      },
      {
        time: "1978-01-01T00:00:00Z",
        fractionalDate: 1978.0,
        uncorr: 6199,
        corr: 6343,
        pressure: 1003.55
      },
      {
        time: "1978-02-01T00:00:00Z",
        fractionalDate: 1978.0849315,
        uncorr: 5580,
        corr: 6312,
        pressure: 1017.27
      },
      {
        time: "1978-03-01T00:00:00Z",
        fractionalDate: 1978.1616438,
        uncorr: 6020,
        corr: 6298,
        pressure: 1006.35
      },
      {
        time: "1978-04-01T00:00:00Z",
        fractionalDate: 1978.2465753,
        uncorr: 5584,
        corr: 6185,
        pressure: 1014.18
      },
      {
        time: "1978-05-01T00:00:00Z",
        fractionalDate: 1978.3287671,
        uncorr: 5199,
        corr: 6052,
        pressure: 1020.57
      },
      {
        time: "1978-06-01T00:00:00Z",
        fractionalDate: 1978.4136986,
        uncorr: 5777,
        corr: 6152,
        pressure: 1008.66
      },
      {
        time: "1978-07-01T00:00:00Z",
        fractionalDate: 1978.4958904,
        uncorr: 5791,
        corr: 6160,
        pressure: 1008.51
      },
      {
        time: "1978-08-01T00:00:00Z",
        fractionalDate: 1978.5808219,
        uncorr: 5869,
        corr: 6310,
        pressure: 1009.91
      },
      {
        time: "1978-09-01T00:00:00Z",
        fractionalDate: 1978.6657534,
        uncorr: 6207,
        corr: 6318,
        pressure: 1002.95
      },
      {
        time: "1978-10-01T00:00:00Z",
        fractionalDate: 1978.7479452,
        uncorr: 6123,
        corr: 6221,
        pressure: 1002.72
      },
      {
        time: "1978-11-01T00:00:00Z",
        fractionalDate: 1978.8328767,
        uncorr: 6204,
        corr: 6263,
        pressure: 1002.07
      },
      {
        time: "1978-12-01T00:00:00Z",
        fractionalDate: 1978.9150685,
        uncorr: 5543,
        corr: 6270,
        pressure: 1017.11
      },
      {
        time: "1979-01-01T00:00:00Z",
        fractionalDate: 1979.0,
        uncorr: 5612,
        corr: 6180,
        pressure: 1013.86
      },
      {
        time: "1979-02-01T00:00:00Z",
        fractionalDate: 1979.0849315,
        uncorr: 5810,
        corr: 6153,
        pressure: 1009.25
      },
      {
        time: "1979-03-01T00:00:00Z",
        fractionalDate: 1979.1616438,
        uncorr: 5854,
        corr: 6078,
        pressure: 1005.37
      },
      {
        time: "1979-04-01T00:00:00Z",
        fractionalDate: 1979.2465753,
        uncorr: 5386,
        corr: 5953,
        pressure: 1013.94
      },
      {
        time: "1979-05-01T00:00:00Z",
        fractionalDate: 1979.3287671,
        uncorr: 5540,
        corr: 5997,
        pressure: 1010.97
      },
      {
        time: "1979-06-01T00:00:00Z",
        fractionalDate: 1979.4136986,
        uncorr: 5432,
        corr: 5858,
        pressure: 1010.44
      },
      {
        time: "1979-07-01T00:00:00Z",
        fractionalDate: 1979.4958904,
        uncorr: 5559,
        corr: 5864,
        pressure: 1007.37
      },
      {
        time: "1979-08-01T00:00:00Z",
        fractionalDate: 1979.5808219,
        uncorr: 5330,
        corr: 5726,
        pressure: 1009.86
      },
      {
        time: "1979-09-01T00:00:00Z",
        fractionalDate: 1979.6657534,
        uncorr: 5683,
        corr: 5780,
        pressure: 1002.7
      },
      {
        time: "1979-10-01T00:00:00Z",
        fractionalDate: 1979.7479452,
        uncorr: 5218,
        corr: 5883,
        pressure: 1016.65
      },
      {
        time: "1979-11-01T00:00:00Z",
        fractionalDate: 1979.8328767,
        uncorr: 5677,
        corr: 5890,
        pressure: 1005.72
      },
      {
        time: "1979-12-01T00:00:00Z",
        fractionalDate: 1979.9150685,
        uncorr: 5911,
        corr: 6000,
        pressure: 1002.72
      },
      {
        time: "1980-01-01T00:00:00Z",
        fractionalDate: 1980.0,
        uncorr: 5403,
        corr: 5974,
        pressure: 1014.19
      },
      {
        time: "1980-02-01T00:00:00Z",
        fractionalDate: 1980.0846995,
        uncorr: 5271,
        corr: 5934,
        pressure: 1016.67
      },
      {
        time: "1980-03-01T00:00:00Z",
        fractionalDate: 1980.1639344,
        uncorr: 5259,
        corr: 6015,
        pressure: 1018.92
      },
      {
        time: "1980-04-01T00:00:00Z",
        fractionalDate: 1980.2486339,
        uncorr: 5411,
        corr: 5912,
        pressure: 1012.44
      },
      {
        time: "1980-05-01T00:00:00Z",
        fractionalDate: 1980.3306011,
        uncorr: 5268,
        corr: 5915,
        pressure: 1015.92
      },
      {
        time: "1980-06-01T00:00:00Z",
        fractionalDate: 1980.4153005,
        uncorr: 5352,
        corr: 5744,
        pressure: 1009.67
      },
      {
        time: "1980-07-01T00:00:00Z",
        fractionalDate: 1980.4972678,
        uncorr: 5367,
        corr: 5744,
        pressure: 1009.32
      },
      {
        time: "1980-08-01T00:00:00Z",
        fractionalDate: 1980.5819672,
        uncorr: 5381,
        corr: 5764,
        pressure: 1009.67
      },
      {
        time: "1980-09-01T00:00:00Z",
        fractionalDate: 1980.6666667,
        uncorr: 5316,
        corr: 5760,
        pressure: 1011.1
      },
      {
        time: "1980-10-01T00:00:00Z",
        fractionalDate: 1980.7486339,
        uncorr: 5435,
        corr: 5652,
        pressure: 1005.5
      },
      {
        time: "1980-11-01T00:00:00Z",
        fractionalDate: 1980.8333333,
        uncorr: 5403,
        corr: 5558,
        pressure: 1004.73
      },
      {
        time: "1980-12-01T00:00:00Z",
        fractionalDate: 1980.9153005,
        uncorr: 5753,
        corr: 5549,
        pressure: 995.43
      },
      {
        time: "1981-01-01T00:00:00Z",
        fractionalDate: 1981.0,
        uncorr: 5817,
        corr: 5733,
        pressure: 998.71
      },
      {
        time: "1981-02-01T00:00:00Z",
        fractionalDate: 1981.0849315,
        uncorr: 5064,
        corr: 5646,
        pressure: 1016.47
      },
      {
        time: "1981-03-01T00:00:00Z",
        fractionalDate: 1981.1616438,
        uncorr: 5407,
        corr: 5619,
        pressure: 1005.78
      },
      {
        time: "1981-04-01T00:00:00Z",
        fractionalDate: 1981.2465753,
        uncorr: 5185,
        corr: 5560,
        pressure: 1009.69
      },
      {
        time: "1981-05-01T00:00:00Z",
        fractionalDate: 1981.3287671,
        uncorr: 4937,
        corr: 5489,
        pressure: 1014.71
      },
      {
        time: "1981-06-01T00:00:00Z",
        fractionalDate: 1981.4136986,
        uncorr: 5337,
        corr: 5646,
        pressure: 1007.88
      },
      {
        time: "1981-07-01T00:00:00Z",
        fractionalDate: 1981.4958904,
        uncorr: 5373,
        corr: 5665,
        pressure: 1007.36
      },
      {
        time: "1981-08-01T00:00:00Z",
        fractionalDate: 1981.5808219,
        uncorr: 5308,
        corr: 5671,
        pressure: 1009.11
      },
      {
        time: "1981-09-01T00:00:00Z",
        fractionalDate: 1981.6657534,
        uncorr: 5195,
        corr: 5753,
        pressure: 1014.01
      },
      {
        time: "1981-10-01T00:00:00Z",
        fractionalDate: 1981.7479452,
        uncorr: 5471,
        corr: 5572,
        pressure: 1002.82
      },
      {
        time: "1981-11-01T00:00:00Z",
        fractionalDate: 1981.8328767,
        uncorr: 5373,
        corr: 5596,
        pressure: 1006.08
      },
      {
        time: "1981-12-01T00:00:00Z",
        fractionalDate: 1981.9150685,
        uncorr: 5654,
        corr: 5711,
        pressure: 1002.18
      },
      {
        time: "1982-01-01T00:00:00Z",
        fractionalDate: 1982.0,
        uncorr: 5525,
        corr: 5830,
        pressure: 1008.1
      },
      {
        time: "1982-02-01T00:00:00Z",
        fractionalDate: 1982.0849315,
        uncorr: 5046,
        corr: 5629,
        pressure: 1015.22
      },
      {
        time: "1982-03-01T00:00:00Z",
        fractionalDate: 1982.1616438,
        uncorr: 5475,
        corr: 5816,
        pressure: 1008.75
      },
      {
        time: "1982-04-01T00:00:00Z",
        fractionalDate: 1982.2465753,
        uncorr: 5725,
        corr: 5861,
        pressure: 1003.7
      },
      {
        time: "1982-05-01T00:00:00Z",
        fractionalDate: 1982.3287671,
        uncorr: 5466,
        corr: 5917,
        pressure: 1010.99
      },
      {
        time: "1982-06-01T00:00:00Z",
        fractionalDate: 1982.4136986,
        uncorr: 5225,
        corr: 5629,
        pressure: 1010.07
      },
      {
        time: "1982-07-01T00:00:00Z",
        fractionalDate: 1982.4958904,
        uncorr: 4938,
        corr: 5415,
        pressure: 1012.56
      },
      {
        time: "1982-08-01T00:00:00Z",
        fractionalDate: 1982.5808219,
        uncorr: 5134,
        corr: 5387,
        pressure: 1006.68
      },
      {
        time: "1982-09-01T00:00:00Z",
        fractionalDate: 1982.6657534,
        uncorr: 4999,
        corr: 5259,
        pressure: 1007.24
      },
      {
        time: "1982-10-01T00:00:00Z",
        fractionalDate: 1982.7479452,
        uncorr: 4888,
        corr: 5400,
        pressure: 1013.84
      },
      {
        time: "1982-11-01T00:00:00Z",
        fractionalDate: 1982.8328767,
        uncorr: 5535,
        corr: 5463,
        pressure: 998.69
      },
      {
        time: "1982-12-01T00:00:00Z",
        fractionalDate: 1982.9150685,
        uncorr: 5515,
        corr: 5367,
        pressure: 997.31
      },
      {
        time: "1983-01-01T00:00:00Z",
        fractionalDate: 1983.0,
        uncorr: 5917,
        corr: 5513,
        pressure: 991.13
      },
      {
        time: "1983-02-01T00:00:00Z",
        fractionalDate: 1983.0849315,
        uncorr: 5141,
        corr: 5645,
        pressure: 1012.91
      },
      {
        time: "1983-03-01T00:00:00Z",
        fractionalDate: 1983.1616438,
        uncorr: 5572,
        corr: 5762,
        pressure: 1005.04
      },
      {
        time: "1983-04-01T00:00:00Z",
        fractionalDate: 1983.2465753,
        uncorr: 5324,
        corr: 5769,
        pressure: 1011.05
      },
      {
        time: "1983-05-01T00:00:00Z",
        fractionalDate: 1983.3287671,
        uncorr: 5121,
        corr: 5602,
        pressure: 1012.26
      },
      {
        time: "1983-06-01T00:00:00Z",
        fractionalDate: 1983.4136986,
        uncorr: 5282,
        corr: 5708,
        pressure: 1010.61
      },
      {
        time: "1983-07-01T00:00:00Z",
        fractionalDate: 1983.4958904,
        uncorr: 5441,
        corr: 5819,
        pressure: 1009.31
      },
      {
        time: "1983-08-01T00:00:00Z",
        fractionalDate: 1983.5808219,
        uncorr: 5498,
        corr: 5823,
        pressure: 1008.02
      },
      {
        time: "1983-09-01T00:00:00Z",
        fractionalDate: 1983.6657534,
        uncorr: 5648,
        corr: 5864,
        pressure: 1005.24
      },
      {
        time: "1983-10-01T00:00:00Z",
        fractionalDate: 1983.7479452,
        uncorr: 6156,
        corr: 5894,
        pressure: 994.46
      },
      {
        time: "1983-11-01T00:00:00Z",
        fractionalDate: 1983.8328767,
        uncorr: 5933,
        corr: 5923,
        pressure: 1000.57
      },
      {
        time: "1983-12-01T00:00:00Z",
        fractionalDate: 1983.9150685,
        uncorr: 5949,
        corr: 5925,
        pressure: 999.98
      },
      {
        time: "1984-01-01T00:00:00Z",
        fractionalDate: 1984.0,
        uncorr: 6043,
        corr: 5977,
        pressure: 1000.21
      },
      {
        time: "1984-02-01T00:00:00Z",
        fractionalDate: 1984.0846995,
        uncorr: 5054,
        corr: 5953,
        pressure: 1023.16
      },
      {
        time: "1984-03-01T00:00:00Z",
        fractionalDate: 1984.1639344,
        uncorr: 5084,
        corr: 5865,
        pressure: 1019.97
      },
      {
        time: "1984-04-01T00:00:00Z",
        fractionalDate: 1984.2486339,
        uncorr: 5143,
        corr: 5803,
        pressure: 1016.62
      },
      {
        time: "1984-05-01T00:00:00Z",
        fractionalDate: 1984.3306011,
        uncorr: 5145,
        corr: 5673,
        pressure: 1013.38
      },
      {
        time: "1984-06-01T00:00:00Z",
        fractionalDate: 1984.4153005,
        uncorr: 5436,
        corr: 5780,
        pressure: 1008.5
      },
      {
        time: "1984-07-01T00:00:00Z",
        fractionalDate: 1984.4972678,
        uncorr: 5610,
        corr: 5813,
        pressure: 1004.92
      },
      {
        time: "1984-08-01T00:00:00Z",
        fractionalDate: 1984.5819672,
        uncorr: 5452,
        corr: 5895,
        pressure: 1010.87
      },
      {
        time: "1984-09-01T00:00:00Z",
        fractionalDate: 1984.6666667,
        uncorr: 5563,
        corr: 5921,
        pressure: 1008.71
      },
      {
        time: "1984-10-01T00:00:00Z",
        fractionalDate: 1984.7486339,
        uncorr: 5747,
        corr: 5921,
        pressure: 1004.46
      },
      {
        time: "1984-11-01T00:00:00Z",
        fractionalDate: 1984.8333333,
        uncorr: 5312,
        corr: 5902,
        pressure: 1014.7
      },
      {
        time: "1984-12-01T00:00:00Z",
        fractionalDate: 1984.9153005,
        uncorr: 5156,
        corr: 5928,
        pressure: 1019.5
      },
      {
        time: "1985-01-01T00:00:00Z",
        fractionalDate: 1985.0,
        uncorr: 5407,
        corr: 5946,
        pressure: 1013.3
      },
      {
        time: "1985-02-01T00:00:00Z",
        fractionalDate: 1985.0849315,
        uncorr: 5192,
        corr: 6035,
        pressure: 1020.71
      },
      {
        time: "1985-03-01T00:00:00Z",
        fractionalDate: 1985.1616438,
        uncorr: 5525,
        corr: 6052,
        pressure: 1013.33
      },
      {
        time: "1985-04-01T00:00:00Z",
        fractionalDate: 1985.2465753,
        uncorr: 5904,
        corr: 6078,
        pressure: 1004.21
      },
      {
        time: "1985-05-01T00:00:00Z",
        fractionalDate: 1985.3287671,
        uncorr: 5442,
        corr: 6100,
        pressure: 1015.58
      },
      {
        time: "1985-06-01T00:00:00Z",
        fractionalDate: 1985.4136986,
        uncorr: 5859,
        corr: 6171,
        pressure: 1007.1
      },
      {
        time: "1985-07-01T00:00:00Z",
        fractionalDate: 1985.4958904,
        uncorr: 5712,
        corr: 6147,
        pressure: 1010.03
      },
      {
        time: "1985-08-01T00:00:00Z",
        fractionalDate: 1985.5808219,
        uncorr: 5886,
        corr: 6149,
        pressure: 1006.09
      },
      {
        time: "1985-09-01T00:00:00Z",
        fractionalDate: 1985.6657534,
        uncorr: 6120,
        corr: 6230,
        pressure: 1002.68
      },
      {
        time: "1985-10-01T00:00:00Z",
        fractionalDate: 1985.7479452,
        uncorr: 5992,
        corr: 6271,
        pressure: 1005.49
      },
      {
        time: "1985-11-01T00:00:00Z",
        fractionalDate: 1985.8328767,
        uncorr: 6001,
        corr: 6340,
        pressure: 1008.49
      },
      {
        time: "1985-12-01T00:00:00Z",
        fractionalDate: 1985.9150685,
        uncorr: 6164,
        corr: 6295,
        pressure: 1002.23
      },
      {
        time: "1986-01-01T00:00:00Z",
        fractionalDate: 1986.0,
        uncorr: 5995,
        corr: 6277,
        pressure: 1006.07
      },
      {
        time: "1986-02-01T00:00:00Z",
        fractionalDate: 1986.0849315,
        uncorr: 5218,
        corr: 6160,
        pressure: 1021.8
      },
      {
        time: "1986-03-01T00:00:00Z",
        fractionalDate: 1986.1616438,
        uncorr: 5894,
        corr: 6261,
        pressure: 1008.4
      },
      {
        time: "1986-04-01T00:00:00Z",
        fractionalDate: 1986.2465753,
        uncorr: 5797,
        corr: 6390,
        pressure: 1012.57
      },
      {
        time: "1986-05-01T00:00:00Z",
        fractionalDate: 1986.3287671,
        uncorr: 5926,
        corr: 6408,
        pressure: 1009.84
      },
      {
        time: "1986-06-01T00:00:00Z",
        fractionalDate: 1986.4136986,
        uncorr: 5803,
        corr: 6418,
        pressure: 1012.77
      },
      {
        time: "1986-07-01T00:00:00Z",
        fractionalDate: 1986.4958904,
        uncorr: 6110,
        corr: 6424,
        pressure: 1006.05
      },
      {
        time: "1986-08-01T00:00:00Z",
        fractionalDate: 1986.5808219,
        uncorr: 5932,
        corr: 6432,
        pressure: 1010.16
      },
      {
        time: "1986-09-01T00:00:00Z",
        fractionalDate: 1986.6657534,
        uncorr: 6295,
        corr: 6451,
        pressure: 1002.64
      },
      {
        time: "1986-10-01T00:00:00Z",
        fractionalDate: 1986.7479452,
        uncorr: 6378,
        corr: 6484,
        pressure: 1001.83
      },
      {
        time: "1986-11-01T00:00:00Z",
        fractionalDate: 1986.8328767,
        uncorr: 6287,
        corr: 6392,
        pressure: 1001.62
      },
      {
        time: "1986-12-01T00:00:00Z",
        fractionalDate: 1986.9150685,
        uncorr: 6019,
        corr: 6498,
        pressure: 1010.01
      },
      {
        time: "1987-01-01T00:00:00Z",
        fractionalDate: 1987.0,
        uncorr: 5833,
        corr: 6581,
        pressure: 1016.47
      },
      {
        time: "1987-02-01T00:00:00Z",
        fractionalDate: 1987.0849315,
        uncorr: 6485,
        corr: 6603,
        pressure: 1001.99
      },
      {
        time: "1987-03-01T00:00:00Z",
        fractionalDate: 1987.1616438,
        uncorr: 5751,
        corr: 6612,
        pressure: 1018.9
      },
      {
        time: "1987-04-01T00:00:00Z",
        fractionalDate: 1987.2465753,
        uncorr: 5990,
        corr: 6592,
        pressure: 1012.32
      },
      {
        time: "1987-05-01T00:00:00Z",
        fractionalDate: 1987.3287671,
        uncorr: 5990,
        corr: 6550,
        pressure: 1011.45
      },
      {
        time: "1987-06-01T00:00:00Z",
        fractionalDate: 1987.4136986,
        uncorr: 6076,
        corr: 6441,
        pressure: 1007.11
      },
      {
        time: "1987-07-01T00:00:00Z",
        fractionalDate: 1987.4958904,
        uncorr: 6018,
        corr: 6393,
        pressure: 1007.59
      },
      {
        time: "1987-08-01T00:00:00Z",
        fractionalDate: 1987.5808219,
        uncorr: 5988,
        corr: 6337,
        pressure: 1006.91
      },
      {
        time: "1987-09-01T00:00:00Z",
        fractionalDate: 1987.6657534,
        uncorr: 6030,
        corr: 6287,
        pressure: 1005.18
      },
      {
        time: "1987-10-01T00:00:00Z",
        fractionalDate: 1987.7479452,
        uncorr: 5415,
        corr: 6291,
        pressure: 1019.69
      },
      {
        time: "1987-11-01T00:00:00Z",
        fractionalDate: 1987.8328767,
        uncorr: 5658,
        corr: 6245,
        pressure: 1012.63
      },
      {
        time: "1987-12-01T00:00:00Z",
        fractionalDate: 1987.9150685,
        uncorr: 6147,
        corr: 6254,
        pressure: 1001.78
      },
      {
        time: "1988-01-01T00:00:00Z",
        fractionalDate: 1988.0,
        uncorr: 5762,
        corr: 6082,
        pressure: 1007.28
      },
      {
        time: "1988-02-01T00:00:00Z",
        fractionalDate: 1988.0846995,
        uncorr: 5659,
        corr: 6144,
        pressure: 1011.01
      },
      {
        time: "1988-03-01T00:00:00Z",
        fractionalDate: 1988.1639344,
        uncorr: 5792,
        corr: 6168,
        pressure: 1007.91
      },
      {
        time: "1988-04-01T00:00:00Z",
        fractionalDate: 1988.2486339,
        uncorr: 5735,
        corr: 6137,
        pressure: 1008.49
      },
      {
        time: "1988-05-01T00:00:00Z",
        fractionalDate: 1988.3306011,
        uncorr: 5456,
        corr: 6152,
        pressure: 1015.8
      },
      {
        time: "1988-06-01T00:00:00Z",
        fractionalDate: 1988.4153005,
        uncorr: 5635,
        corr: 6128,
        pressure: 1010.52
      },
      {
        time: "1988-07-01T00:00:00Z",
        fractionalDate: 1988.4972678,
        uncorr: 5620,
        corr: 6018,
        pressure: 1008.39
      },
      {
        time: "1988-08-01T00:00:00Z",
        fractionalDate: 1988.5819672,
        uncorr: 5800,
        corr: 6021,
        pressure: 1004.23
      },
      {
        time: "1988-09-01T00:00:00Z",
        fractionalDate: 1988.6666667,
        uncorr: 5874,
        corr: 6016,
        pressure: 1002.75
      },
      {
        time: "1988-10-01T00:00:00Z",
        fractionalDate: 1988.7486339,
        uncorr: 5602,
        corr: 5992,
        pressure: 1009.07
      },
      {
        time: "1988-11-01T00:00:00Z",
        fractionalDate: 1988.8333333,
        uncorr: 5860,
        corr: 5969,
        pressure: 1002.01
      },
      {
        time: "1988-12-01T00:00:00Z",
        fractionalDate: 1988.9153005,
        uncorr: 5838,
        corr: 5812,
        pressure: 998.82
      },
      {
        time: "1989-01-01T00:00:00Z",
        fractionalDate: 1989.0,
        uncorr: 5818,
        corr: 5737,
        pressure: 997.71
      },
      {
        time: "1989-02-01T00:00:00Z",
        fractionalDate: 1989.0849315,
        uncorr: 5967,
        corr: 5718,
        pressure: 994.03
      },
      {
        time: "1989-03-01T00:00:00Z",
        fractionalDate: 1989.1616438,
        uncorr: 5349,
        corr: 5450,
        pressure: 1001.8
      },
      {
        time: "1989-04-01T00:00:00Z",
        fractionalDate: 1989.2465753,
        uncorr: 4855,
        corr: 5505,
        pressure: 1016.31
      },
      {
        time: "1989-05-01T00:00:00Z",
        fractionalDate: 1989.3287671,
        uncorr: 5031,
        corr: 5402,
        pressure: 1008.98
      },
      {
        time: "1989-06-01T00:00:00Z",
        fractionalDate: 1989.4136986,
        uncorr: 4944,
        corr: 5445,
        pressure: 1012.46
      },
      {
        time: "1989-07-01T00:00:00Z",
        fractionalDate: 1989.4958904,
        uncorr: 5195,
        corr: 5615,
        pressure: 1009.84
      },
      {
        time: "1989-08-01T00:00:00Z",
        fractionalDate: 1989.5808219,
        uncorr: 5299,
        corr: 5538,
        pressure: 1005.16
      },
      {
        time: "1989-09-01T00:00:00Z",
        fractionalDate: 1989.6657534,
        uncorr: 5098,
        corr: 5471,
        pressure: 1008.83
      },
      {
        time: "1989-10-01T00:00:00Z",
        fractionalDate: 1989.7479452,
        uncorr: 5195,
        corr: 5355,
        pressure: 1003.61
      },
      {
        time: "1989-11-01T00:00:00Z",
        fractionalDate: 1989.8328767,
        uncorr: 4897,
        corr: 5226,
        pressure: 1008.12
      },
      {
        time: "1989-12-01T00:00:00Z",
        fractionalDate: 1989.9150685,
        uncorr: 5283,
        corr: 5317,
        pressure: 1000.54
      },
      {
        time: "1990-01-01T00:00:00Z",
        fractionalDate: 1990.0,
        uncorr: 5267,
        corr: 5420,
        pressure: 1003.64
      },
      {
        time: "1990-02-01T00:00:00Z",
        fractionalDate: 1990.0849315,
        uncorr: 5860,
        corr: 5442,
        pressure: 989.86
      },
      {
        time: "1990-03-01T00:00:00Z",
        fractionalDate: 1990.1616438,
        uncorr: 5529,
        corr: 5373,
        pressure: 996.07
      },
      {
        time: "1990-04-01T00:00:00Z",
        fractionalDate: 1990.2465753,
        uncorr: 4900,
        corr: 5267,
        pressure: 1009.25
      },
      {
        time: "1990-05-01T00:00:00Z",
        fractionalDate: 1990.3287671,
        uncorr: 4816,
        corr: 5247,
        pressure: 1010.85
      },
      {
        time: "1990-06-01T00:00:00Z",
        fractionalDate: 1990.4136986,
        uncorr: 4868,
        corr: 5236,
        pressure: 1009.02
      },
      {
        time: "1990-07-01T00:00:00Z",
        fractionalDate: 1990.4958904,
        uncorr: 5009,
        corr: 5381,
        pressure: 1008.9
      },
      {
        time: "1990-08-01T00:00:00Z",
        fractionalDate: 1990.5808219,
        uncorr: 4939,
        corr: 5353,
        pressure: 1010.12
      },
      {
        time: "1990-09-01T00:00:00Z",
        fractionalDate: 1990.6657534,
        uncorr: 5015,
        corr: 5454,
        pressure: 1010.72
      },
      {
        time: "1990-10-01T00:00:00Z",
        fractionalDate: 1990.7479452,
        uncorr: 5236,
        corr: 5551,
        pressure: 1007.46
      },
      {
        time: "1990-11-01T00:00:00Z",
        fractionalDate: 1990.8328767,
        uncorr: 5431,
        corr: 5630,
        pressure: 1004.42
      },
      {
        time: "1990-12-01T00:00:00Z",
        fractionalDate: 1990.9150685,
        uncorr: 5482,
        corr: 5645,
        pressure: 1003.8
      },
      {
        time: "1991-01-01T00:00:00Z",
        fractionalDate: 1991.0,
        uncorr: 5482,
        corr: 5775,
        pressure: 1006.8
      },
      {
        time: "1991-02-01T00:00:00Z",
        fractionalDate: 1991.0849315,
        uncorr: 5021,
        corr: 5809,
        pressure: 1020.38
      },
      {
        time: "1991-03-01T00:00:00Z",
        fractionalDate: 1991.1616438,
        uncorr: 4881,
        corr: 5421,
        pressure: 1013.59
      },
      {
        time: "1991-04-01T00:00:00Z",
        fractionalDate: 1991.2465753,
        uncorr: 4972,
        corr: 5470,
        pressure: 1012.5
      },
      {
        time: "1991-05-01T00:00:00Z",
        fractionalDate: 1991.3287671,
        uncorr: 5124,
        corr: 5501,
        pressure: 1008.89
      },
      {
        time: "1991-06-01T00:00:00Z",
        fractionalDate: 1991.4136986,
        uncorr: 4648,
        corr: 4879,
        pressure: 1005.7
      },
      {
        time: "1991-07-01T00:00:00Z",
        fractionalDate: 1991.4958904,
        uncorr: 4570,
        corr: 4920,
        pressure: 1009.25
      },
      {
        time: "1991-08-01T00:00:00Z",
        fractionalDate: 1991.5808219,
        uncorr: 4889,
        corr: 5228,
        pressure: 1008.38
      },
      {
        time: "1991-09-01T00:00:00Z",
        fractionalDate: 1991.6657534,
        uncorr: 5350,
        corr: 5473,
        pressure: 1002.52
      },
      {
        time: "1991-10-01T00:00:00Z",
        fractionalDate: 1991.7479452,
        uncorr: 5101,
        corr: 5535,
        pressure: 1010.95
      },
      {
        time: "1991-11-01T00:00:00Z",
        fractionalDate: 1991.8328767,
        uncorr: 5556,
        corr: 5552,
        pressure: 999.41
      },
      {
        time: "1991-12-01T00:00:00Z",
        fractionalDate: 1991.9150685,
        uncorr: 5560,
        corr: 5649,
        pressure: 1002.16
      },
      {
        time: "1992-01-01T00:00:00Z",
        fractionalDate: 1992.0,
        uncorr: 5572,
        corr: 5632,
        pressure: 1001.32
      },
      {
        time: "1992-02-01T00:00:00Z",
        fractionalDate: 1992.0846995,
        uncorr: 5522,
        corr: 5591,
        pressure: 1001.22
      },
      {
        time: "1992-03-01T00:00:00Z",
        fractionalDate: 1992.1639344,
        uncorr: 5665,
        corr: 5724,
        pressure: 1001.25
      },
      {
        time: "1992-04-01T00:00:00Z",
        fractionalDate: 1992.2486339,
        uncorr: 5532,
        corr: 5895,
        pressure: 1007.83
      },
      {
        time: "1992-05-01T00:00:00Z",
        fractionalDate: 1992.3306011,
        uncorr: 5331,
        corr: 5847,
        pressure: 1012.07
      },
      {
        time: "1992-06-01T00:00:00Z",
        fractionalDate: 1992.4153005,
        uncorr: 5396,
        corr: 5984,
        pressure: 1013.43
      },
      {
        time: "1992-07-01T00:00:00Z",
        fractionalDate: 1992.4972678,
        uncorr: 5728,
        corr: 6060,
        pressure: 1006.85
      },
      {
        time: "1992-08-01T00:00:00Z",
        fractionalDate: 1992.5819672,
        uncorr: 5801,
        corr: 6029,
        pressure: 1004.55
      },
      {
        time: "1992-09-01T00:00:00Z",
        fractionalDate: 1992.6666667,
        uncorr: 5417,
        corr: 5995,
        pressure: 1013.21
      },
      {
        time: "1992-10-01T00:00:00Z",
        fractionalDate: 1992.7486339,
        uncorr: 5731,
        corr: 6095,
        pressure: 1007.68
      },
      {
        time: "1992-11-01T00:00:00Z",
        fractionalDate: 1992.8333333,
        uncorr: 5851,
        corr: 6057,
        pressure: 1004.11
      },
      {
        time: "1992-12-01T00:00:00Z",
        fractionalDate: 1992.9153005,
        uncorr: 6041,
        corr: 6150,
        pressure: 1002.38
      },
      {
        time: "1993-01-01T00:00:00Z",
        fractionalDate: 1993.0,
        uncorr: 6536,
        corr: 6126,
        pressure: 991.61
      },
      {
        time: "1993-02-01T00:00:00Z",
        fractionalDate: 1993.0849315,
        uncorr: 5773,
        corr: 6130,
        pressure: 1008.0
      },
      {
        time: "1993-03-01T00:00:00Z",
        fractionalDate: 1993.1616438,
        uncorr: 5780,
        corr: 6049,
        pressure: 1006.33
      },
      {
        time: "1993-04-01T00:00:00Z",
        fractionalDate: 1993.2465753,
        uncorr: 5552,
        corr: 6144,
        pressure: 1013.35
      },
      {
        time: "1993-05-01T00:00:00Z",
        fractionalDate: 1993.3287671,
        uncorr: 5510,
        corr: 6181,
        pressure: 1014.99
      },
      {
        time: "1993-06-01T00:00:00Z",
        fractionalDate: 1993.4136986,
        uncorr: 5917,
        corr: 6213,
        pressure: 1005.78
      },
      {
        time: "1993-07-01T00:00:00Z",
        fractionalDate: 1993.4958904,
        uncorr: 5964,
        corr: 6225,
        pressure: 1005.1
      },
      {
        time: "1993-08-01T00:00:00Z",
        fractionalDate: 1993.5808219,
        uncorr: 5776,
        corr: 6233,
        pressure: 1009.49
      },
      {
        time: "1993-09-01T00:00:00Z",
        fractionalDate: 1993.6657534,
        uncorr: 5659,
        corr: 6274,
        pressure: 1013.29
      },
      {
        time: "1993-10-01T00:00:00Z",
        fractionalDate: 1993.7479452,
        uncorr: 5858,
        corr: 6277,
        pressure: 1008.97
      },
      {
        time: "1993-11-01T00:00:00Z",
        fractionalDate: 1993.8328767,
        uncorr: 5026,
        corr: 6302,
        pressure: 1030.02
      },
      {
        time: "1993-12-01T00:00:00Z",
        fractionalDate: 1993.9150685,
        uncorr: 6423,
        corr: 6289,
        pressure: 997.3
      },
      {
        time: "1994-01-01T00:00:00Z",
        fractionalDate: 1994.0,
        uncorr: 6341,
        corr: 6300,
        pressure: 998.7
      },
      {
        time: "1994-02-01T00:00:00Z",
        fractionalDate: 1994.0849315,
        uncorr: 5282,
        corr: 6196,
        pressure: 1021.07
      },
      {
        time: "1994-03-01T00:00:00Z",
        fractionalDate: 1994.1616438,
        uncorr: 6271,
        corr: 6202,
        pressure: 998.32
      },
      {
        time: "1994-04-01T00:00:00Z",
        fractionalDate: 1994.2465753,
        uncorr: 5813,
        corr: 6182,
        pressure: 1007.62
      },
      {
        time: "1994-05-01T00:00:00Z",
        fractionalDate: 1994.3287671,
        uncorr: 5673,
        corr: 6248,
        pressure: 1012.4
      },
      {
        time: "1994-06-01T00:00:00Z",
        fractionalDate: 1994.4136986,
        uncorr: 6177,
        corr: 6255,
        pressure: 1000.94
      },
      {
        time: "1994-07-01T00:00:00Z",
        fractionalDate: 1994.4958904,
        uncorr: 5629,
        corr: 6287,
        pressure: 1014.09
      },
      {
        time: "1994-08-01T00:00:00Z",
        fractionalDate: 1994.5808219,
        uncorr: 5856,
        corr: 6339,
        pressure: 1010.06
      },
      {
        time: "1994-09-01T00:00:00Z",
        fractionalDate: 1994.6657534,
        uncorr: 5984,
        corr: 6370,
        pressure: 1007.99
      },
      {
        time: "1994-10-01T00:00:00Z",
        fractionalDate: 1994.7479452,
        uncorr: 6154,
        corr: 6336,
        pressure: 1003.43
      },
      {
        time: "1994-11-01T00:00:00Z",
        fractionalDate: 1994.8328767,
        uncorr: 6102,
        corr: 6329,
        pressure: 1004.86
      },
      {
        time: "1994-12-01T00:00:00Z",
        fractionalDate: 1994.9150685,
        uncorr: 6269,
        corr: 6315,
        pressure: 1000.93
      },
      {
        time: "1995-01-01T00:00:00Z",
        fractionalDate: 1995.0,
        uncorr: 6185,
        corr: 6363,
        pressure: 1003.35
      },
      {
        time: "1995-02-01T00:00:00Z",
        fractionalDate: 1995.0849315,
        uncorr: 7067,
        corr: 6382,
        pressure: 985.15
      },
      {
        time: "1995-03-01T00:00:00Z",
        fractionalDate: 1995.1616438,
        uncorr: 6271,
        corr: 6350,
        pressure: 1001.34
      },
      {
        time: "1995-04-01T00:00:00Z",
        fractionalDate: 1995.2465753,
        uncorr: 5932,
        corr: 6376,
        pressure: 1008.9
      },
      {
        time: "1995-05-01T00:00:00Z",
        fractionalDate: 1995.3287671,
        uncorr: 5734,
        corr: 6395,
        pressure: 1013.39
      },
      {
        time: "1995-06-01T00:00:00Z",
        fractionalDate: 1995.4136986,
        uncorr: 5906,
        corr: 6398,
        pressure: 1009.45
      },
      {
        time: "1995-07-01T00:00:00Z",
        fractionalDate: 1995.4958904,
        uncorr: 6017,
        corr: 6403,
        pressure: 1007.16
      },
      {
        time: "1995-08-01T00:00:00Z",
        fractionalDate: 1995.5808219,
        uncorr: 5964,
        corr: 6412,
        pressure: 1008.44
      },
      {
        time: "1995-09-01T00:00:00Z",
        fractionalDate: 1995.6657534,
        uncorr: 5880,
        corr: 6399,
        pressure: 1010.44
      },
      {
        time: "1995-10-01T00:00:00Z",
        fractionalDate: 1995.7479452,
        uncorr: 6119,
        corr: 6371,
        pressure: 1004.26
      },
      {
        time: "1995-11-01T00:00:00Z",
        fractionalDate: 1995.8328767,
        uncorr: 6025,
        corr: 6378,
        pressure: 1006.57
      },
      {
        time: "1995-12-01T00:00:00Z",
        fractionalDate: 1995.9150685,
        uncorr: 5786,
        corr: 6425,
        pressure: 1013.73
      },
      {
        time: "1996-01-01T00:00:00Z",
        fractionalDate: 1996.0,
        uncorr: 5341,
        corr: 6433,
        pressure: 1023.92
      },
      {
        time: "1996-02-01T00:00:00Z",
        fractionalDate: 1996.0846995,
        uncorr: 5886,
        corr: 6457,
        pressure: 1011.59
      },
      {
        time: "1996-03-01T00:00:00Z",
        fractionalDate: 1996.1639344,
        uncorr: 5473,
        corr: 6512,
        pressure: 1022.73
      },
      {
        time: "1996-04-01T00:00:00Z",
        fractionalDate: 1996.2486339,
        uncorr: 5776,
        corr: 6534,
        pressure: 1015.46
      },
      {
        time: "1996-05-01T00:00:00Z",
        fractionalDate: 1996.3306011,
        uncorr: 5886,
        corr: 6534,
        pressure: 1012.92
      },
      {
        time: "1996-06-01T00:00:00Z",
        fractionalDate: 1996.4153005,
        uncorr: 6094,
        corr: 6539,
        pressure: 1008.1
      },
      {
        time: "1996-07-01T00:00:00Z",
        fractionalDate: 1996.4972678,
        uncorr: 6209,
        corr: 6542,
        pressure: 1005.82
      },
      {
        time: "1996-08-01T00:00:00Z",
        fractionalDate: 1996.5819672,
        uncorr: 5657,
        corr: 6519,
        pressure: 1017.8
      },
      {
        time: "1996-09-01T00:00:00Z",
        fractionalDate: 1996.6666667,
        uncorr: 5784,
        corr: 6514,
        pressure: 1014.74
      },
      {
        time: "1996-10-01T00:00:00Z",
        fractionalDate: 1996.7486339,
        uncorr: 6127,
        corr: 6484,
        pressure: 1006.66
      },
      {
        time: "1996-11-01T00:00:00Z",
        fractionalDate: 1996.8333333,
        uncorr: 6430,
        corr: 6477,
        pressure: 999.95
      },
      {
        time: "1996-12-01T00:00:00Z",
        fractionalDate: 1996.9153005,
        uncorr: 6073,
        corr: 6489,
        pressure: 1007.96
      },
      {
        time: "1997-01-01T00:00:00Z",
        fractionalDate: 1997.0,
        uncorr: 6251,
        corr: 6533,
        pressure: 1004.86
      },
      {
        time: "1997-02-01T00:00:00Z",
        fractionalDate: 1997.0849315,
        uncorr: 6681,
        corr: 6552,
        pressure: 996.75
      },
      {
        time: "1997-03-01T00:00:00Z",
        fractionalDate: 1997.1616438,
        uncorr: 6389,
        corr: 6559,
        pressure: 1002.76
      },
      {
        time: "1997-04-01T00:00:00Z",
        fractionalDate: 1997.2465753,
        uncorr: 6396,
        corr: 6543,
        pressure: 1002.08
      },
      {
        time: "1997-05-01T00:00:00Z",
        fractionalDate: 1997.3287671,
        uncorr: 5981,
        corr: 6559,
        pressure: 1011.41
      },
      {
        time: "1997-06-01T00:00:00Z",
        fractionalDate: 1997.4136986,
        uncorr: 5996,
        corr: 6550,
        pressure: 1010.68
      },
      {
        time: "1997-07-01T00:00:00Z",
        fractionalDate: 1997.4958904,
        uncorr: 5853,
        corr: 6547,
        pressure: 1013.76
      },
      {
        time: "1997-08-01T00:00:00Z",
        fractionalDate: 1997.5808219,
        uncorr: 5784,
        corr: 6580,
        pressure: 1016.0
      },
      {
        time: "1997-09-01T00:00:00Z",
        fractionalDate: 1997.6657534,
        uncorr: 6340,
        corr: 6575,
        pressure: 1003.91
      },
      {
        time: "1997-10-01T00:00:00Z",
        fractionalDate: 1997.7479452,
        uncorr: 6401,
        corr: 6538,
        pressure: 1001.65
      },
      {
        time: "1997-11-01T00:00:00Z",
        fractionalDate: 1997.8328767,
        uncorr: 5781,
        corr: 6489,
        pressure: 1014.61
      },
      {
        time: "1997-12-01T00:00:00Z",
        fractionalDate: 1997.9150685,
        uncorr: 5942,
        corr: 6518,
        pressure: 1011.4
      },
      {
        time: "1998-01-01T00:00:00Z",
        fractionalDate: 1998.0,
        uncorr: 6210,
        corr: 6526,
        pressure: 1005.67
      },
      {
        time: "1998-02-01T00:00:00Z",
        fractionalDate: 1998.0849315,
        uncorr: 6576,
        corr: 6537,
        pressure: 998.45
      },
      {
        time: "1998-03-01T00:00:00Z",
        fractionalDate: 1998.1616438,
        uncorr: 6029,
        corr: 6543,
        pressure: 1010.2
      },
      {
        time: "1998-04-01T00:00:00Z",
        fractionalDate: 1998.2465753,
        uncorr: 5766,
        corr: 6390,
        pressure: 1012.53
      },
      {
        time: "1998-05-01T00:00:00Z",
        fractionalDate: 1998.3287671,
        uncorr: 5780,
        corr: 6280,
        pressure: 1009.95
      },
      {
        time: "1998-06-01T00:00:00Z",
        fractionalDate: 1998.4136986,
        uncorr: 5817,
        corr: 6308,
        pressure: 1009.62
      },
      {
        time: "1998-07-01T00:00:00Z",
        fractionalDate: 1998.4958904,
        uncorr: 6255,
        corr: 6382,
        pressure: 1001.28
      },
      {
        time: "1998-08-01T00:00:00Z",
        fractionalDate: 1998.5808219,
        uncorr: 5991,
        corr: 6300,
        pressure: 1005.54
      },
      {
        time: "1998-09-01T00:00:00Z",
        fractionalDate: 1998.6657534,
        uncorr: 5788,
        corr: 6380,
        pressure: 1012.26
      },
      {
        time: "1998-10-01T00:00:00Z",
        fractionalDate: 1998.7479452,
        uncorr: 6446,
        corr: 6434,
        pressure: 999.19
      },
      {
        time: "1998-11-01T00:00:00Z",
        fractionalDate: 1998.8328767,
        uncorr: 5582,
        corr: 6386,
        pressure: 1017.01
      },
      {
        time: "1998-12-01T00:00:00Z",
        fractionalDate: 1998.9150685,
        uncorr: 6135,
        corr: 6338,
        pressure: 1003.55
      },
      {
        time: "1999-01-01T00:00:00Z",
        fractionalDate: 1999.0,
        uncorr: 5956,
        corr: 6250,
        pressure: 1005.34
      },
      {
        time: "1999-02-01T00:00:00Z",
        fractionalDate: 1999.0849315,
        uncorr: 6279,
        corr: 6249,
        pressure: 998.54
      },
      {
        time: "1999-03-01T00:00:00Z",
        fractionalDate: 1999.1616438,
        uncorr: 5678,
        corr: 6264,
        pressure: 1012.27
      },
      {
        time: "1999-04-01T00:00:00Z",
        fractionalDate: 1999.2465753,
        uncorr: 5910,
        corr: 6314,
        pressure: 1007.87
      },
      {
        time: "1999-05-01T00:00:00Z",
        fractionalDate: 1999.3287671,
        uncorr: 5631,
        corr: 6292,
        pressure: 1013.63
      },
      {
        time: "1999-06-01T00:00:00Z",
        fractionalDate: 1999.4136986,
        uncorr: 5722,
        corr: 6342,
        pressure: 1012.55
      },
      {
        time: "1999-07-01T00:00:00Z",
        fractionalDate: 1999.4958904,
        uncorr: 5993,
        corr: 6386,
        pressure: 1007.27
      },
      {
        time: "1999-08-01T00:00:00Z",
        fractionalDate: 1999.5808219,
        uncorr: 5801,
        corr: 6243,
        pressure: 1008.5
      },
      {
        time: "1999-09-01T00:00:00Z",
        fractionalDate: 1999.6657534,
        uncorr: 5526,
        corr: 6121,
        pressure: 1012.67
      },
      {
        time: "1999-10-01T00:00:00Z",
        fractionalDate: 1999.7479452,
        uncorr: 5751,
        corr: 6064,
        pressure: 1006.35
      },
      {
        time: "1999-11-01T00:00:00Z",
        fractionalDate: 1999.8328767,
        uncorr: 5673,
        corr: 6040,
        pressure: 1007.59
      },
      {
        time: "1999-12-01T00:00:00Z",
        fractionalDate: 1999.9150685,
        uncorr: 6234,
        corr: 5951,
        pressure: 992.77
      },
      {
        time: "2000-01-01T00:00:00Z",
        fractionalDate: 2000.0,
        uncorr: 6163,
        corr: 6014,
        pressure: 996.21
      },
      {
        time: "2000-02-01T00:00:00Z",
        fractionalDate: 2000.0846995,
        uncorr: 6096,
        corr: 5962,
        pressure: 996.49
      },
      {
        time: "2000-03-01T00:00:00Z",
        fractionalDate: 2000.1639344,
        uncorr: 5918,
        corr: 5881,
        pressure: 998.62
      },
      {
        time: "2000-04-01T00:00:00Z",
        fractionalDate: 2000.2486339,
        uncorr: 5471,
        corr: 5901,
        pressure: 1009.42
      },
      {
        time: "2000-05-01T00:00:00Z",
        fractionalDate: 2000.3306011,
        uncorr: 5330,
        corr: 5780,
        pressure: 1009.87
      },
      {
        time: "2000-06-01T00:00:00Z",
        fractionalDate: 2000.4153005,
        uncorr: 5395,
        corr: 5671,
        pressure: 1005.69
      },
      {
        time: "2000-07-01T00:00:00Z",
        fractionalDate: 2000.4972678,
        uncorr: 5235,
        corr: 5569,
        pressure: 1007.29
      },
      {
        time: "2000-08-01T00:00:00Z",
        fractionalDate: 2000.5819672,
        uncorr: 5300,
        corr: 5655,
        pressure: 1007.63
      },
      {
        time: "2000-09-01T00:00:00Z",
        fractionalDate: 2000.6666667,
        uncorr: 5117,
        corr: 5718,
        pressure: 1014.11
      },
      {
        time: "2000-10-01T00:00:00Z",
        fractionalDate: 2000.7486339,
        uncorr: 5251,
        corr: 5833,
        pressure: 1013.52
      },
      {
        time: "2000-11-01T00:00:00Z",
        fractionalDate: 2000.8333333,
        uncorr: 5169,
        corr: 5694,
        pressure: 1012.04
      },
      {
        time: "2000-12-01T00:00:00Z",
        fractionalDate: 2000.9153005,
        uncorr: 5501,
        corr: 5743,
        pressure: 1004.83
      },
      {
        time: "2001-01-01T00:00:00Z",
        fractionalDate: 2001.0,
        uncorr: 5313,
        corr: 5817,
        pressure: 1011.37
      },
      {
        time: "2001-02-01T00:00:00Z",
        fractionalDate: 2001.0849315,
        uncorr: 5551,
        corr: 5960,
        pressure: 1008.95
      },
      {
        time: "2001-03-01T00:00:00Z",
        fractionalDate: 2001.1616438,
        uncorr: 5721,
        corr: 6025,
        pressure: 1006.01
      },
      {
        time: "2001-04-01T00:00:00Z",
        fractionalDate: 2001.2465753,
        uncorr: 5389,
        corr: 5737,
        pressure: 1007.54
      },
      {
        time: "2001-05-01T00:00:00Z",
        fractionalDate: 2001.3287671,
        uncorr: 5480,
        corr: 5885,
        pressure: 1008.61
      },
      {
        time: "2001-06-01T00:00:00Z",
        fractionalDate: 2001.4136986,
        uncorr: 5575,
        corr: 5929,
        pressure: 1007.23
      },
      {
        time: "2001-07-01T00:00:00Z",
        fractionalDate: 2001.4958904,
        uncorr: 5536,
        corr: 5959,
        pressure: 1008.88
      },
      {
        time: "2001-08-01T00:00:00Z",
        fractionalDate: 2001.5808219,
        uncorr: 5423,
        corr: 5857,
        pressure: 1009.42
      },
      {
        time: "2001-09-01T00:00:00Z",
        fractionalDate: 2001.6657534,
        uncorr: 5326,
        corr: 5857,
        pressure: 1011.86
      },
      {
        time: "2001-10-01T00:00:00Z",
        fractionalDate: 2001.7479452,
        uncorr: 5471,
        corr: 5771,
        pressure: 1006.31
      },
      {
        time: "2001-11-01T00:00:00Z",
        fractionalDate: 2001.8328767,
        uncorr: 5777,
        corr: 5855,
        pressure: 1001.41
      },
      {
        time: "2001-12-01T00:00:00Z",
        fractionalDate: 2001.9150685,
        uncorr: 5277,
        corr: 5907,
        pressure: 1015.0
      },
      {
        time: "2002-01-01T00:00:00Z",
        fractionalDate: 2002.0,
        uncorr: 5641,
        corr: 5736,
        pressure: 1001.47
      },
      {
        time: "2002-02-01T00:00:00Z",
        fractionalDate: 2002.0849315,
        uncorr: 6381,
        corr: 5910,
        pressure: 988.9
      },
      {
        time: "2002-03-01T00:00:00Z",
        fractionalDate: 2002.1616438,
        uncorr: 5716,
        corr: 5846,
        pressure: 1002.94
      },
      {
        time: "2002-04-01T00:00:00Z",
        fractionalDate: 2002.2465753,
        uncorr: 5108,
        corr: 5849,
        pressure: 1017.25
      },
      {
        time: "2002-05-01T00:00:00Z",
        fractionalDate: 2002.3287671,
        uncorr: 5235,
        corr: 5859,
        pressure: 1014.11
      },
      {
        time: "2002-06-01T00:00:00Z",
        fractionalDate: 2002.4136986,
        uncorr: 5531,
        corr: 5909,
        pressure: 1007.87
      },
      {
        time: "2002-07-01T00:00:00Z",
        fractionalDate: 2002.4958904,
        uncorr: 5371,
        corr: 5809,
        pressure: 1009.64
      },
      {
        time: "2002-08-01T00:00:00Z",
        fractionalDate: 2002.5808219,
        uncorr: 5070,
        corr: 5685,
        pressure: 1014.4
      },
      {
        time: "2002-09-01T00:00:00Z",
        fractionalDate: 2002.6657534,
        uncorr: 5296,
        corr: 5780,
        pressure: 1010.85
      },
      {
        time: "2002-10-01T00:00:00Z",
        fractionalDate: 2002.7479452,
        uncorr: 5281,
        corr: 5826,
        pressure: 1012.56
      },
      {
        time: "2002-11-01T00:00:00Z",
        fractionalDate: 2002.8328767,
        uncorr: 5067,
        corr: 5729,
        pressure: 1015.83
      },
      {
        time: "2002-12-01T00:00:00Z",
        fractionalDate: 2002.9150685,
        uncorr: 4904,
        corr: 5756,
        pressure: 1021.22
      },
      {
        time: "2003-01-01T00:00:00Z",
        fractionalDate: 2003.0,
        uncorr: 5779,
        corr: 5810,
        pressure: 999.99
      },
      {
        time: "2003-02-01T00:00:00Z",
        fractionalDate: 2003.0849315,
        uncorr: 5067,
        corr: 5808,
        pressure: 1017.44
      },
      {
        time: "2003-03-01T00:00:00Z",
        fractionalDate: 2003.1616438,
        uncorr: 5358,
        corr: 5845,
        pressure: 1011.0
      },
      {
        time: "2003-04-01T00:00:00Z",
        fractionalDate: 2003.2465753,
        uncorr: 5023,
        corr: 5822,
        pressure: 1019.19
      },
      {
        time: "2003-05-01T00:00:00Z",
        fractionalDate: 2003.3287671,
        uncorr: 5363,
        corr: 5772,
        pressure: 1008.8
      },
      {
        time: "2003-06-01T00:00:00Z",
        fractionalDate: 2003.4136986,
        uncorr: 5243,
        corr: 5654,
        pressure: 1009.24
      },
      {
        time: "2003-07-01T00:00:00Z",
        fractionalDate: 2003.4958904,
        uncorr: 5212,
        corr: 5750,
        pressure: 1012.31
      },
      {
        time: "2003-08-01T00:00:00Z",
        fractionalDate: 2003.5808219,
        uncorr: 5454,
        corr: 5800,
        pressure: 1007.46
      },
      {
        time: "2003-09-01T00:00:00Z",
        fractionalDate: 2003.6657534,
        uncorr: 5424,
        corr: 5850,
        pressure: 1009.52
      },
      {
        time: "2003-10-01T00:00:00Z",
        fractionalDate: 2003.7479452,
        uncorr: 5589,
        corr: 5762,
        pressure: 1003.6
      },
      {
        time: "2003-11-01T00:00:00Z",
        fractionalDate: 2003.8328767,
        uncorr: 4863,
        corr: 5435,
        pressure: 1014.3
      },
      {
        time: "2003-12-01T00:00:00Z",
        fractionalDate: 2003.9150685,
        uncorr: 5860,
        corr: 5801,
        pressure: 998.22
      },
      {
        time: "2004-01-01T00:00:00Z",
        fractionalDate: 2004.0,
        uncorr: 5440,
        corr: 5755,
        pressure: 1006.87
      },
      {
        time: "2004-02-01T00:00:00Z",
        fractionalDate: 2004.0846995,
        uncorr: 5754,
        corr: 5923,
        pressure: 1003.07
      },
      {
        time: "2004-03-01T00:00:00Z",
        fractionalDate: 2004.1639344,
        uncorr: 5413,
        corr: 6043,
        pressure: 1014.44
      },
      {
        time: "2004-04-01T00:00:00Z",
        fractionalDate: 2004.2486339,
        uncorr: 5429,
        corr: 6106,
        pressure: 1015.12
      },
      {
        time: "2004-05-01T00:00:00Z",
        fractionalDate: 2004.3306011,
        uncorr: 5792,
        corr: 6166,
        pressure: 1007.61
      },
      {
        time: "2004-06-01T00:00:00Z",
        fractionalDate: 2004.4153005,
        uncorr: 5870,
        corr: 6163,
        pressure: 1005.79
      },
      {
        time: "2004-07-01T00:00:00Z",
        fractionalDate: 2004.4972678,
        uncorr: 5612,
        corr: 6077,
        pressure: 1010.03
      },
      {
        time: "2004-08-01T00:00:00Z",
        fractionalDate: 2004.5819672,
        uncorr: 5650,
        corr: 6114,
        pressure: 1009.91
      },
      {
        time: "2004-09-01T00:00:00Z",
        fractionalDate: 2004.6666667,
        uncorr: 6018,
        corr: 6154,
        pressure: 1002.48
      },
      {
        time: "2004-10-01T00:00:00Z",
        fractionalDate: 2004.7486339,
        uncorr: 5798,
        corr: 6296,
        pressure: 1010.44
      },
      {
        time: "2004-11-01T00:00:00Z",
        fractionalDate: 2004.8333333,
        uncorr: 5998,
        corr: 6136,
        pressure: 1002.36
      },
      {
        time: "2004-12-01T00:00:00Z",
        fractionalDate: 2004.9153005,
        uncorr: 6314,
        corr: 6182,
        pressure: 996.74
      },
      {
        time: "2005-01-01T00:00:00Z",
        fractionalDate: 2005.0,
        uncorr: 6091,
        corr: 5959,
        pressure: 996.61
      },
      {
        time: "2005-02-01T00:00:00Z",
        fractionalDate: 2005.0849315,
        uncorr: 5414,
        corr: 6134,
        pressure: 1016.41
      },
      {
        time: "2005-03-01T00:00:00Z",
        fractionalDate: 2005.1616438,
        uncorr: 5623,
        corr: 6179,
        pressure: 1011.88
      },
      {
        time: "2005-04-01T00:00:00Z",
        fractionalDate: 2005.2465753,
        uncorr: 5736,
        corr: 6233,
        pressure: 1010.47
      },
      {
        time: "2005-05-01T00:00:00Z",
        fractionalDate: 2005.3287671,
        uncorr: 5687,
        corr: 6092,
        pressure: 1008.4
      },
      {
        time: "2005-06-01T00:00:00Z",
        fractionalDate: 2005.4136986,
        uncorr: 5745,
        corr: 6207,
        pressure: 1009.55
      },
      {
        time: "2005-07-01T00:00:00Z",
        fractionalDate: 2005.4958904,
        uncorr: 5718,
        corr: 6166,
        pressure: 1009.24
      },
      {
        time: "2005-08-01T00:00:00Z",
        fractionalDate: 2005.5808219,
        uncorr: 5717,
        corr: 6114,
        pressure: 1008.12
      },
      {
        time: "2005-09-01T00:00:00Z",
        fractionalDate: 2005.6657534,
        uncorr: 5629,
        corr: 5945,
        pressure: 1006.54
      },
      {
        time: "2005-10-01T00:00:00Z",
        fractionalDate: 2005.7479452,
        uncorr: 5578,
        corr: 6233,
        pressure: 1014.29
      },
      {
        time: "2005-11-01T00:00:00Z",
        fractionalDate: 2005.8328767,
        uncorr: 6167,
        corr: 6307,
        pressure: 1002.59
      },
      {
        time: "2005-12-01T00:00:00Z",
        fractionalDate: 2005.9150685,
        uncorr: 5769,
        corr: 6305,
        pressure: 1011.85
      },
      {
        time: "2006-01-01T00:00:00Z",
        fractionalDate: 2006.0,
        uncorr: 5433,
        corr: 6330,
        pressure: 1020.29
      },
      {
        time: "2006-02-01T00:00:00Z",
        fractionalDate: 2006.0849315,
        uncorr: 5779,
        corr: 6405,
        pressure: 1013.02
      },
      {
        time: "2006-03-01T00:00:00Z",
        fractionalDate: 2006.1616438,
        uncorr: 5976,
        corr: 6479,
        pressure: 1010.47
      },
      {
        time: "2006-04-01T00:00:00Z",
        fractionalDate: 2006.2465753,
        uncorr: 6047,
        corr: 6488,
        pressure: 1008.8
      },
      {
        time: "2006-05-01T00:00:00Z",
        fractionalDate: 2006.3287671,
        uncorr: 6016,
        corr: 6515,
        pressure: 1010.43
      },
      {
        time: "2006-06-01T00:00:00Z",
        fractionalDate: 2006.4136986,
        uncorr: 5937,
        corr: 6512,
        pressure: 1011.53
      },
      {
        time: "2006-07-01T00:00:00Z",
        fractionalDate: 2006.4958904,
        uncorr: 5834,
        corr: 6495,
        pressure: 1013.51
      },
      {
        time: "2006-08-01T00:00:00Z",
        fractionalDate: 2006.5808219,
        uncorr: 5999,
        corr: 6499,
        pressure: 1009.81
      },
      {
        time: "2006-09-01T00:00:00Z",
        fractionalDate: 2006.6657534,
        uncorr: 6106,
        corr: 6495,
        pressure: 1007.51
      },
      {
        time: "2006-10-01T00:00:00Z",
        fractionalDate: 2006.7479452,
        uncorr: 6185,
        corr: 6547,
        pressure: 1007.05
      },
      {
        time: "2006-11-01T00:00:00Z",
        fractionalDate: 2006.8328767,
        uncorr: 6357,
        corr: 6529,
        pressure: 1002.72
      },
      {
        time: "2006-12-01T00:00:00Z",
        fractionalDate: 2006.9150685,
        uncorr: 6559,
        corr: 6446,
        pressure: 997.03
      },
      {
        time: "2007-01-01T00:00:00Z",
        fractionalDate: 2007.0,
        uncorr: 7042,
        corr: 6578,
        pressure: 990.11
      },
      {
        time: "2007-02-01T00:00:00Z",
        fractionalDate: 2007.0849315,
        uncorr: 5878,
        corr: 6551,
        pressure: 1013.92
      },
      {
        time: "2007-03-01T00:00:00Z",
        fractionalDate: 2007.1616438,
        uncorr: 6201,
        corr: 6589,
        pressure: 1008.15
      },
      {
        time: "2007-04-01T00:00:00Z",
        fractionalDate: 2007.2465753,
        uncorr: 6339,
        corr: 6651,
        pressure: 1005.99
      },
      {
        time: "2007-05-01T00:00:00Z",
        fractionalDate: 2007.3287671,
        uncorr: 6415,
        corr: 6651,
        pressure: 1004.11
      },
      {
        time: "2007-06-01T00:00:00Z",
        fractionalDate: 2007.4136986,
        uncorr: 6224,
        corr: 6655,
        pressure: 1008.58
      },
      {
        time: "2007-07-01T00:00:00Z",
        fractionalDate: 2007.4958904,
        uncorr: 6440,
        corr: 6645,
        pressure: 1003.27
      },
      {
        time: "2007-08-01T00:00:00Z",
        fractionalDate: 2007.5808219,
        uncorr: 6226,
        corr: 6638,
        pressure: 1007.85
      },
      {
        time: "2007-09-01T00:00:00Z",
        fractionalDate: 2007.6657534,
        uncorr: 6349,
        corr: 6652,
        pressure: 1005.65
      },
      {
        time: "2007-10-01T00:00:00Z",
        fractionalDate: 2007.7479452,
        uncorr: 6004,
        corr: 6657,
        pressure: 1013.36
      },
      {
        time: "2007-11-01T00:00:00Z",
        fractionalDate: 2007.8328767,
        uncorr: 6442,
        corr: 6648,
        pressure: 1003.49
      },
      {
        time: "2007-12-01T00:00:00Z",
        fractionalDate: 2007.9150685,
        uncorr: 6324,
        corr: 6671,
        pressure: 1007.05
      },
      {
        time: "2008-01-01T00:00:00Z",
        fractionalDate: 2008.0,
        uncorr: 6359,
        corr: 6646,
        pressure: 1006.26
      },
      {
        time: "2008-02-01T00:00:00Z",
        fractionalDate: 2008.0846995,
        uncorr: 6455,
        corr: 6630,
        pressure: 1003.79
      },
      {
        time: "2008-03-01T00:00:00Z",
        fractionalDate: 2008.1639344,
        uncorr: 6724,
        corr: 6631,
        pressure: 997.29
      },
      {
        time: "2008-04-01T00:00:00Z",
        fractionalDate: 2008.2486339,
        uncorr: 6004,
        corr: 6640,
        pressure: 1012.68
      },
      {
        time: "2008-05-01T00:00:00Z",
        fractionalDate: 2008.3306011,
        uncorr: 5854,
        corr: 6631,
        pressure: 1015.92
      },
      {
        time: "2008-06-01T00:00:00Z",
        fractionalDate: 2008.4153005,
        uncorr: 6295,
        corr: 6636,
        pressure: 1006.25
      },
      {
        time: "2008-07-01T00:00:00Z",
        fractionalDate: 2008.4972678,
        uncorr: 6197,
        corr: 6652,
        pressure: 1008.76
      },
      {
        time: "2008-08-01T00:00:00Z",
        fractionalDate: 2008.5819672,
        uncorr: 6372,
        corr: 6656,
        pressure: 1005.67
      },
      {
        time: "2008-09-01T00:00:00Z",
        fractionalDate: 2008.6666667,
        uncorr: 5939,
        corr: 6679,
        pressure: 1016.38
      },
      {
        time: "2008-10-01T00:00:00Z",
        fractionalDate: 2008.7486339,
        uncorr: 6777,
        corr: 6699,
        pressure: 998.28
      },
      {
        time: "2008-11-01T00:00:00Z",
        fractionalDate: 2008.8333333,
        uncorr: 6806,
        corr: 6724,
        pressure: 998.83
      },
      {
        time: "2008-12-01T00:00:00Z",
        fractionalDate: 2008.9153005,
        uncorr: 6163,
        corr: 6723,
        pressure: 1011.86
      },
      {
        time: "2009-01-01T00:00:00Z",
        fractionalDate: 2009.0,
        uncorr: 6415,
        corr: 6738,
        pressure: 1006.82
      },
      {
        time: "2009-02-01T00:00:00Z",
        fractionalDate: 2009.0849315,
        uncorr: 6283,
        corr: 6737,
        pressure: 1009.55
      },
      {
        time: "2009-03-01T00:00:00Z",
        fractionalDate: 2009.1616438,
        uncorr: 6366,
        corr: 6780,
        pressure: 1008.32
      },
      {
        time: "2009-04-01T00:00:00Z",
        fractionalDate: 2009.2465753,
        uncorr: 6126,
        corr: 6819,
        pressure: 1014.25
      },
      {
        time: "2009-05-01T00:00:00Z",
        fractionalDate: 2009.3287671,
        uncorr: 6283,
        corr: 6826,
        pressure: 1011.21
      },
      {
        time: "2009-06-01T00:00:00Z",
        fractionalDate: 2009.4136986,
        uncorr: 6237,
        corr: 6814,
        pressure: 1011.89
      },
      {
        time: "2009-07-01T00:00:00Z",
        fractionalDate: 2009.4958904,
        uncorr: 6428,
        corr: 6805,
        pressure: 1007.41
      },
      {
        time: "2009-08-01T00:00:00Z",
        fractionalDate: 2009.5808219,
        uncorr: 6306,
        corr: 6804,
        pressure: 1010.23
      },
      {
        time: "2009-09-01T00:00:00Z",
        fractionalDate: 2009.6657534,
        uncorr: 6640,
        corr: 6819,
        pressure: 1003.59
      },
      {
        time: "2009-10-01T00:00:00Z",
        fractionalDate: 2009.7479452,
        uncorr: 6320,
        corr: 6839,
        pressure: 1011.03
      },
      {
        time: "2009-11-01T00:00:00Z",
        fractionalDate: 2009.8328767,
        uncorr: 6532,
        corr: 6834,
        pressure: 1006.45
      },
      {
        time: "2009-12-01T00:00:00Z",
        fractionalDate: 2009.9150685,
        uncorr: 6216,
        corr: 6844,
        pressure: 1013.26
      },
      {
        time: "2010-01-01T00:00:00Z",
        fractionalDate: 2010.0,
        uncorr: 5847,
        corr: 6811,
        pressure: 1021.2
      },
      {
        time: "2010-02-01T00:00:00Z",
        fractionalDate: 2010.0849315,
        uncorr: 6228,
        corr: 6718,
        pressure: 1010.24
      },
      {
        time: "2010-03-01T00:00:00Z",
        fractionalDate: 2010.1616438,
        uncorr: 6513,
        corr: 6656,
        pressure: 1002.97
      },
      {
        time: "2010-04-01T00:00:00Z",
        fractionalDate: 2010.2465753,
        uncorr: 6082,
        corr: 6600,
        pressure: 1011.23
      },
      {
        time: "2010-05-01T00:00:00Z",
        fractionalDate: 2010.3287671,
        uncorr: 6130,
        corr: 6630,
        pressure: 1010.68
      },
      {
        time: "2010-06-01T00:00:00Z",
        fractionalDate: 2010.4136986,
        uncorr: 6208,
        corr: 6610,
        pressure: 1008.36
      },
      {
        time: "2010-07-01T00:00:00Z",
        fractionalDate: 2010.4958904,
        uncorr: 6084,
        corr: 6600,
        pressure: 1010.82
      },
      {
        time: "2010-08-01T00:00:00Z",
        fractionalDate: 2010.5808219,
        uncorr: 6137,
        corr: 6578,
        pressure: 1009.31
      },
      {
        time: "2010-09-01T00:00:00Z",
        fractionalDate: 2010.6657534,
        uncorr: 6051,
        corr: 6578,
        pressure: 1011.39
      },
      {
        time: "2010-10-01T00:00:00Z",
        fractionalDate: 2010.7479452,
        uncorr: 6337,
        corr: 6604,
        pressure: 1005.92
      },
      {
        time: "2010-11-01T00:00:00Z",
        fractionalDate: 2010.8328767,
        uncorr: 6250,
        corr: 6575,
        pressure: 1007.72
      },
      {
        time: "2010-12-01T00:00:00Z",
        fractionalDate: 2010.9150685,
        uncorr: 5981,
        corr: 6534,
        pressure: 1012.24
      },
      {
        time: "2011-01-01T00:00:00Z",
        fractionalDate: 2011.0,
        uncorr: 6322,
        corr: 6558,
        pressure: 1005.04
      },
      {
        time: "2011-02-01T00:00:00Z",
        fractionalDate: 2011.0849315,
        uncorr: 5860,
        corr: 6530,
        pressure: 1015.45
      },
      {
        time: "2011-03-01T00:00:00Z",
        fractionalDate: 2011.1616438,
        uncorr: 6270,
        corr: 6508,
        pressure: 1005.58
      },
      {
        time: "2011-04-01T00:00:00Z",
        fractionalDate: 2011.2465753,
        uncorr: 5982,
        corr: 6385,
        pressure: 1008.88
      },
      {
        time: "2011-05-01T00:00:00Z",
        fractionalDate: 2011.3287671,
        uncorr: 5902,
        corr: 6459,
        pressure: 1012.32
      },
      {
        time: "2011-06-01T00:00:00Z",
        fractionalDate: 2011.4136986,
        uncorr: 5859,
        corr: 6293,
        pressure: 1009.56
      },
      {
        time: "2011-07-01T00:00:00Z",
        fractionalDate: 2011.4958904,
        uncorr: 5878,
        corr: 6347,
        pressure: 1010.19
      },
      {
        time: "2011-08-01T00:00:00Z",
        fractionalDate: 2011.5808219,
        uncorr: 5911,
        corr: 6363,
        pressure: 1009.96
      },
      {
        time: "2011-09-01T00:00:00Z",
        fractionalDate: 2011.6657534,
        uncorr: 6164,
        corr: 6356,
        pressure: 1004.25
      },
      {
        time: "2011-10-01T00:00:00Z",
        fractionalDate: 2011.7479452,
        uncorr: 6153,
        corr: 6321,
        pressure: 1004.34
      },
      {
        time: "2011-11-01T00:00:00Z",
        fractionalDate: 2011.8328767,
        uncorr: 5992,
        corr: 6398,
        pressure: 1009.56
      },
      {
        time: "2011-12-01T00:00:00Z",
        fractionalDate: 2011.9150685,
        uncorr: 6978,
        corr: 6486,
        pressure: 990.24
      },
      {
        time: "2012-01-01T00:00:00Z",
        fractionalDate: 2012.0,
        uncorr: 5884,
        corr: 6395,
        pressure: 1012.95
      },
      {
        time: "2012-02-01T00:00:00Z",
        fractionalDate: 2012.0846995,
        uncorr: 5744,
        corr: 6322,
        pressure: 1014.54
      },
      {
        time: "2012-03-01T00:00:00Z",
        fractionalDate: 2012.1639344,
        uncorr: 5814,
        corr: 6162,
        pressure: 1008.02
      },
      {
        time: "2012-04-01T00:00:00Z",
        fractionalDate: 2012.2486339,
        uncorr: 6189,
        corr: 6368,
        pressure: 1003.73
      },
      {
        time: "2012-05-01T00:00:00Z",
        fractionalDate: 2012.3306011,
        uncorr: 5897,
        corr: 6388,
        pressure: 1010.87
      },
      {
        time: "2012-06-01T00:00:00Z",
        fractionalDate: 2012.4153005,
        uncorr: 6016,
        corr: 6301,
        pressure: 1006.09
      },
      {
        time: "2012-07-01T00:00:00Z",
        fractionalDate: 2012.4972678,
        uncorr: 5840,
        corr: 6117,
        pressure: 1006.18
      },
      {
        time: "2012-08-01T00:00:00Z",
        fractionalDate: 2012.5819672,
        uncorr: 5649,
        corr: 6149,
        pressure: 1011.42
      },
      {
        time: "2012-09-01T00:00:00Z",
        fractionalDate: 2012.6666667,
        uncorr: 6152,
        corr: 6242,
        pressure: 1001.92
      },
      {
        time: "2012-10-01T00:00:00Z",
        fractionalDate: 2012.7486339,
        uncorr: 6073,
        corr: 6235,
        pressure: 1003.73
      },
      {
        time: "2012-11-01T00:00:00Z",
        fractionalDate: 2012.8333333,
        uncorr: 6035,
        corr: 6261,
        pressure: 1005.24
      },
      {
        time: "2012-12-01T00:00:00Z",
        fractionalDate: 2012.9153005,
        uncorr: 5637,
        corr: 6305,
        pressure: 1015.64
      },
      {
        time: "2013-01-01T00:00:00Z",
        fractionalDate: 2013.0,
        uncorr: 6063,
        corr: 6324,
        pressure: 1006.02
      },
      {
        time: "2013-02-01T00:00:00Z",
        fractionalDate: 2013.0849315,
        uncorr: 5735,
        corr: 6327,
        pressure: 1013.75
      },
      {
        time: "2013-03-01T00:00:00Z",
        fractionalDate: 2013.1616438,
        uncorr: 5652,
        corr: 6272,
        pressure: 1014.35
      },
      {
        time: "2013-04-01T00:00:00Z",
        fractionalDate: 2013.2465753,
        uncorr: 5960,
        corr: 6280,
        pressure: 1007.05
      },
      {
        time: "2013-05-01T00:00:00Z",
        fractionalDate: 2013.3287671,
        uncorr: 5556,
        corr: 6103,
        pressure: 1012.58
      },
      {
        time: "2013-06-01T00:00:00Z",
        fractionalDate: 2013.4136986,
        uncorr: 5627,
        corr: 6096,
        pressure: 1010.74
      },
      {
        time: "2013-07-01T00:00:00Z",
        fractionalDate: 2013.4958904,
        uncorr: 5649,
        corr: 6116,
        pressure: 1010.59
      },
      {
        time: "2013-08-01T00:00:00Z",
        fractionalDate: 2013.5808219,
        uncorr: 5668,
        corr: 6154,
        pressure: 1010.98
      },
      {
        time: "2013-09-01T00:00:00Z",
        fractionalDate: 2013.6657534,
        uncorr: 5643,
        corr: 6193,
        pressure: 1012.49
      },
      {
        time: "2013-10-01T00:00:00Z",
        fractionalDate: 2013.7479452,
        uncorr: 6088,
        corr: 6260,
        pressure: 1004.11
      },
      {
        time: "2013-11-01T00:00:00Z",
        fractionalDate: 2013.8328767,
        uncorr: 6370,
        corr: 6230,
        pressure: 997.11
      },
      {
        time: "2013-12-01T00:00:00Z",
        fractionalDate: 2013.9150685,
        uncorr: 6280,
        corr: 6175,
        pressure: 998.01
      },
      {
        time: "2014-01-01T00:00:00Z",
        fractionalDate: 2014.0,
        uncorr: 5414,
        corr: 6204,
        pressure: 1018.76
      },
      {
        time: "2014-02-01T00:00:00Z",
        fractionalDate: 2014.0849315,
        uncorr: 5874,
        corr: 6091,
        pressure: 1004.91
      },
      {
        time: "2014-03-01T00:00:00Z",
        fractionalDate: 2014.1616438,
        uncorr: 5942,
        corr: 6134,
        pressure: 1004.83
      },
      {
        time: "2014-04-01T00:00:00Z",
        fractionalDate: 2014.2465753,
        uncorr: 5732,
        corr: 6163,
        pressure: 1009.99
      },
      {
        time: "2014-05-01T00:00:00Z",
        fractionalDate: 2014.3287671,
        uncorr: 5619,
        corr: 6213,
        pressure: 1013.54
      },
      {
        time: "2014-06-01T00:00:00Z",
        fractionalDate: 2014.4136986,
        uncorr: 5649,
        corr: 6128,
        pressure: 1010.91
      },
      {
        time: "2014-07-01T00:00:00Z",
        fractionalDate: 2014.4958904,
        uncorr: 5551,
        corr: 6170,
        pressure: 1014.21
      },
      {
        time: "2014-08-01T00:00:00Z",
        fractionalDate: 2014.5808219,
        uncorr: 5960,
        corr: 6230,
        pressure: 1005.99
      },
      {
        time: "2014-09-01T00:00:00Z",
        fractionalDate: 2014.6657534,
        uncorr: 5614,
        corr: 6161,
        pressure: 1012.88
      },
      {
        time: "2014-10-01T00:00:00Z",
        fractionalDate: 2014.7479452,
        uncorr: 5512,
        corr: 6164,
        pressure: 1015.4
      },
      {
        time: "2014-11-01T00:00:00Z",
        fractionalDate: 2014.8328767,
        uncorr: 5344,
        corr: 6145,
        pressure: 1019.04
      },
      {
        time: "2014-12-01T00:00:00Z",
        fractionalDate: 2014.9150685,
        uncorr: 6118,
        corr: 6020,
        pressure: 998.21
      },
      {
        time: "2015-01-01T00:00:00Z",
        fractionalDate: 2015.0,
        uncorr: 6145,
        corr: 6071,
        pressure: 999.19
      },
      {
        time: "2015-02-01T00:00:00Z",
        fractionalDate: 2015.0849315,
        uncorr: 6001,
        corr: 6084,
        pressure: 1002.26
      },
      {
        time: "2015-03-01T00:00:00Z",
        fractionalDate: 2015.1616438,
        uncorr: 5529,
        corr: 6003,
        pressure: 1012.09
      },
      {
        time: "2015-04-01T00:00:00Z",
        fractionalDate: 2015.2465753,
        uncorr: 5902,
        corr: 6057,
        pressure: 1003.53
      },
      {
        time: "2015-05-01T00:00:00Z",
        fractionalDate: 2015.3287671,
        uncorr: 5892,
        corr: 6130,
        pressure: 1005.25
      },
      {
        time: "2015-06-01T00:00:00Z",
        fractionalDate: 2015.4136986,
        uncorr: 5891,
        corr: 6114,
        pressure: 1004.93
      },
      {
        time: "2015-07-01T00:00:00Z",
        fractionalDate: 2015.4958904,
        uncorr: 5979,
        corr: 6187,
        pressure: 1004.6
      },
      {
        time: "2015-08-01T00:00:00Z",
        fractionalDate: 2015.5808219,
        uncorr: 5530,
        corr: 6196,
        pressure: 1015.32
      },
      {
        time: "2015-09-01T00:00:00Z",
        fractionalDate: 2015.6657534,
        uncorr: 5619,
        corr: 6197,
        pressure: 1013.5
      },
      {
        time: "2015-10-01T00:00:00Z",
        fractionalDate: 2015.7479452,
        uncorr: 5575,
        corr: 6212,
        pressure: 1014.98
      },
      {
        time: "2015-11-01T00:00:00Z",
        fractionalDate: 2015.8328767,
        uncorr: 6345,
        corr: 6237,
        pressure: 997.81
      },
      {
        time: "2015-12-01T00:00:00Z",
        fractionalDate: 2015.9150685,
        uncorr: 6262,
        corr: 6276,
        pressure: 1001.03
      },
      {
        time: "2016-01-01T00:00:00Z",
        fractionalDate: 2016.0,
        uncorr: 6045,
        corr: 6376,
        pressure: 1008.03
      },
      {
        time: "2016-02-01T00:00:00Z",
        fractionalDate: 2016.0846995,
        uncorr: 6597,
        corr: 6431,
        pressure: 996.91
      },
      {
        time: "2016-03-01T00:00:00Z",
        fractionalDate: 2016.1639344,
        uncorr: 5956,
        corr: 6420,
        pressure: 1010.15
      },
      {
        time: "2016-04-01T00:00:00Z",
        fractionalDate: 2016.2486339,
        uncorr: 6076,
        corr: 6432,
        pressure: 1007.73
      },
      {
        time: "2016-05-01T00:00:00Z",
        fractionalDate: 2016.3306011,
        uncorr: 5775,
        corr: 6438,
        pressure: 1014.63
      },
      {
        time: "2016-06-01T00:00:00Z",
        fractionalDate: 2016.4153005,
        uncorr: 6034,
        corr: 6469,
        pressure: 1009.34
      },
      {
        time: "2016-07-01T00:00:00Z",
        fractionalDate: 2016.4972678,
        uncorr: 6174,
        corr: 6439,
        pressure: 1005.7
      },
      {
        time: "2016-08-01T00:00:00Z",
        fractionalDate: 2016.5819672,
        uncorr: 6110,
        corr: 6485,
        pressure: 1007.98
      },
      {
        time: "2016-09-01T00:00:00Z",
        fractionalDate: 2016.6666667,
        uncorr: 5976,
        corr: 6489,
        pressure: 1011.33
      },
      {
        time: "2016-10-01T00:00:00Z",
        fractionalDate: 2016.7486339,
        uncorr: 5369,
        corr: 6542,
        pressure: 1027.12
      },
      {
        time: "2016-11-01T00:00:00Z",
        fractionalDate: 2016.8333333,
        uncorr: 6117,
        corr: 6583,
        pressure: 1010.15
      },
      {
        time: "2016-12-01T00:00:00Z",
        fractionalDate: 2016.9153005,
        uncorr: 6472,
        corr: 6580,
        pressure: 1002.49
      },
      {
        time: "2017-01-01T00:00:00Z",
        fractionalDate: 2017.0,
        uncorr: 6338,
        corr: 6619,
        pressure: 1006.09
      },
      {
        time: "2017-02-01T00:00:00Z",
        fractionalDate: 2017.0849315,
        uncorr: 6199,
        corr: 6636,
        pressure: 1010.81
      },
      {
        time: "2017-03-01T00:00:00Z",
        fractionalDate: 2017.1616438,
        uncorr: 6510,
        corr: 6654,
        pressure: 1003.28
      },
      {
        time: "2017-04-01T00:00:00Z",
        fractionalDate: 2017.2465753,
        uncorr: 6341,
        corr: 6616,
        pressure: 1005.96
      },
      {
        time: "2017-05-01T00:00:00Z",
        fractionalDate: 2017.3287671,
        uncorr: 5975,
        corr: 6632,
        pressure: 1014.11
      },
      {
        time: "2017-06-01T00:00:00Z",
        fractionalDate: 2017.4136986,
        uncorr: 6446,
        corr: 6649,
        pressure: 1004.16
      },
      {
        time: "2017-07-01T00:00:00Z",
        fractionalDate: 2017.4958904,
        uncorr: 6276,
        corr: 6586,
        pressure: 1006.39
      },
      {
        time: "2017-08-01T00:00:00Z",
        fractionalDate: 2017.5808219,
        uncorr: 6183,
        corr: 6517,
        pressure: 1007.0
      },
      {
        time: "2017-09-01T00:00:00Z",
        fractionalDate: 2017.6657534,
        uncorr: 5724,
        corr: 6474,
        pressure: 1017.38
      },
      {
        time: "2017-10-01T00:00:00Z",
        fractionalDate: 2017.7479452,
        uncorr: 6267,
        corr: 6545,
        pressure: 1006.27
      },
      {
        time: "2017-11-01T00:00:00Z",
        fractionalDate: 2017.8328767,
        uncorr: 6555,
        corr: 6616,
        pressure: 1001.24
      },
      {
        time: "2017-12-01T00:00:00Z",
        fractionalDate: 2017.9150685,
        uncorr: 6803,
        corr: 6631,
        pressure: 996.59
      },
      {
        time: "2018-01-01T00:00:00Z",
        fractionalDate: 2018.0,
        uncorr: 6254,
        corr: 6633,
        pressure: 1008.35
      },
      {
        time: "2018-02-01T00:00:00Z",
        fractionalDate: 2018.0849315,
        uncorr: 5586,
        corr: 6654,
        pressure: 1023.88
      },
      {
        time: "2018-03-01T00:00:00Z",
        fractionalDate: 2018.1616438,
        uncorr: 6257,
        corr: 6662,
        pressure: 1008.56
      },
      {
        time: "2018-04-01T00:00:00Z",
        fractionalDate: 2018.2465753,
        uncorr: 6258,
        corr: 6678,
        pressure: 1008.89
      },
      {
        time: "2018-05-01T00:00:00Z",
        fractionalDate: 2018.3287671,
        uncorr: 5789,
        corr: 6667,
        pressure: 1018.93
      },
      {
        time: "2018-06-01T00:00:00Z",
        fractionalDate: 2018.4136986,
        uncorr: 6407,
        corr: 6707,
        pressure: 1006.2
      },
      {
        time: "2018-07-01T00:00:00Z",
        fractionalDate: 2018.4958904,
        uncorr: 6039,
        corr: 6702,
        pressure: 1013.99
      },
      {
        time: "2018-08-01T00:00:00Z",
        fractionalDate: 2018.5808219,
        uncorr: 6354,
        corr: 6702,
        pressure: 1007.14
      },
      {
        time: "2018-09-01T00:00:00Z",
        fractionalDate: 2018.6657534,
        uncorr: 6508,
        corr: 6712,
        pressure: 1004.59
      },
      {
        time: "2018-10-01T00:00:00Z",
        fractionalDate: 2018.7479452,
        uncorr: 6394,
        corr: 6722,
        pressure: 1007.02
      },
      {
        time: "2018-11-01T00:00:00Z",
        fractionalDate: 2018.8328767,
        uncorr: 5914,
        corr: 6710,
        pressure: 1017.12
      },
      {
        time: "2018-12-01T00:00:00Z",
        fractionalDate: 2018.9150685,
        uncorr: 6215,
        corr: 6698,
        pressure: 1010.59
      },
      {
        time: "2019-01-01T00:00:00Z",
        fractionalDate: 2019.0,
        uncorr: 6660,
        corr: 6700,
        pressure: 1001.21
      },
      {
        time: "2019-02-01T00:00:00Z",
        fractionalDate: 2019.0849315,
        uncorr: 6477,
        corr: 6689,
        pressure: 1004.73
      },
      {
        time: "2019-03-01T00:00:00Z",
        fractionalDate: 2019.1616438,
        uncorr: 6900,
        corr: 6733,
        pressure: 996.75
      },
      {
        time: "2019-04-01T00:00:00Z",
        fractionalDate: 2019.2465753,
        uncorr: 5718,
        corr: 6732,
        pressure: 1022.01
      },
      {
        time: "2019-05-01T00:00:00Z",
        fractionalDate: 2019.3287671,
        uncorr: 6292,
        corr: 6674,
        pressure: 1008.24
      },
      {
        time: "2019-06-01T00:00:00Z",
        fractionalDate: 2019.4136986,
        uncorr: 6192,
        corr: 6719,
        pressure: 1011.0
      },
      {
        time: "2019-07-01T00:00:00Z",
        fractionalDate: 2019.4958904,
        uncorr: 6271,
        corr: 6719,
        pressure: 1009.29
      },
      {
        time: "2019-08-01T00:00:00Z",
        fractionalDate: 2019.5808219,
        uncorr: 6242,
        corr: 6713,
        pressure: 1009.71
      },
      {
        time: "2019-09-01T00:00:00Z",
        fractionalDate: 2019.6657534,
        uncorr: 6395,
        corr: 6739,
        pressure: 1007.17
      },
      {
        time: "2019-10-01T00:00:00Z",
        fractionalDate: 2019.7479452,
        uncorr: 6516,
        corr: 6752,
        pressure: 1004.97
      },
      {
        time: "2019-11-01T00:00:00Z",
        fractionalDate: 2019.8328767,
        uncorr: 6229,
        corr: 6740,
        pressure: 1010.93
      },
      {
        time: "2019-12-01T00:00:00Z",
        fractionalDate: 2019.9150685,
        uncorr: 6935,
        corr: 6743,
        pressure: 996.66
      },
      {
        time: "2020-01-01T00:00:00Z",
        fractionalDate: 2020.0,
        uncorr: 6978,
        corr: 6762,
        pressure: 995.9
      },
      {
        time: "2020-02-01T00:00:00Z",
        fractionalDate: 2020.0846995,
        uncorr: 7460,
        corr: 6793,
        pressure: 988.11
      },
      {
        time: "2020-03-01T00:00:00Z",
        fractionalDate: 2020.1639344,
        uncorr: 6563,
        corr: 6784,
        pressure: 1004.97
      },
      {
        time: "2020-04-01T00:00:00Z",
        fractionalDate: 2020.2486339,
        uncorr: 6594,
        corr: 6813,
        pressure: 1004.88
      },
      {
        time: "2020-05-01T00:00:00Z",
        fractionalDate: 2020.3306011,
        uncorr: 6292,
        corr: 6791,
        pressure: 1010.46
      },
      {
        time: "2020-06-01T00:00:00Z",
        fractionalDate: 2020.4153005,
        uncorr: 6118,
        corr: 6767,
        pressure: 1013.69
      },
      {
        time: "2020-07-01T00:00:00Z",
        fractionalDate: 2020.4972678,
        uncorr: 6564,
        corr: 6760,
        pressure: 1003.97
      },
      {
        time: "2020-08-01T00:00:00Z",
        fractionalDate: 2020.5819672,
        uncorr: 6328,
        corr: 6756,
        pressure: 1008.8
      },
      {
        time: "2020-09-01T00:00:00Z",
        fractionalDate: 2020.6666667,
        uncorr: 6437,
        corr: 6753,
        pressure: 1006.61
      },
      {
        time: "2020-10-01T00:00:00Z",
        fractionalDate: 2020.7486339,
        uncorr: 6340,
        corr: 6746,
        pressure: 1008.52
      },
      {
        time: "2020-11-01T00:00:00Z",
        fractionalDate: 2020.8333333,
        uncorr: 6455,
        corr: 6747,
        pressure: 1006.47
      },
      {
        time: "2020-12-01T00:00:00Z",
        fractionalDate: 2020.9153005,
        uncorr: 6121,
        corr: 6707,
        pressure: 1012.28
      },
      {
        time: "2021-01-01T00:00:00Z",
        fractionalDate: 2021.0,
        uncorr: 6214,
        corr: 6697,
        pressure: 1010.43
      },
      {
        time: "2021-02-01T00:00:00Z",
        fractionalDate: 2021.0849315,
        uncorr: 5872,
        corr: 6621,
        pressure: 1016.28
      },
      {
        time: "2021-03-01T00:00:00Z",
        fractionalDate: 2021.1616438,
        uncorr: 6516,
        corr: 6709,
        pressure: 1004.14
      },
      {
        time: "2021-04-01T00:00:00Z",
        fractionalDate: 2021.2465753,
        uncorr: 6443,
        corr: 6737,
        pressure: 1006.76
      },
      {
        time: "2021-05-01T00:00:00Z",
        fractionalDate: 2021.3287671,
        uncorr: 6332,
        corr: 6730,
        pressure: 1008.2
      },
      {
        time: "2021-06-01T00:00:00Z",
        fractionalDate: 2021.4136986,
        uncorr: 6139,
        corr: 6716,
        pressure: 1012.12
      },
      {
        time: "2021-07-01T00:00:00Z",
        fractionalDate: 2021.4958904,
        uncorr: 6249,
        corr: 6688,
        pressure: 1009.12
      }
    ],
    "timeComment:":
      " Database query duration: 1 seconds. This page was generated in 1 seconds from 688 rows.  "
  };
  