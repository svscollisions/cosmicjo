import * as Tone from "tone";
import { normalizeBetweenTwoRanges } from "./utilities";
import { demo } from "./demo";
console.log("Initilized");
// Prepare data set
const data = demo.data;

const testBtn = document.querySelector(".test");
const startBtn = document.querySelector(".start");
const stopBtn = document.querySelector(".stop");
const activateBtn = document.querySelector(".activate");

// const feedbackDelay = new Tone.FeedbackDelay("8n", 0.5).toDestination();
// const tom = new Tone.MetalSynth({
//   octaves: 4,
//   pitchDecay: 0.1,
// }).connect(feedbackDelay);
const freeverb = new Tone.Freeverb().toDestination();
freeverb.dampening = 10000;
// // routing synth through the reverb
// const synth = new Tone.NoiseSynth().connect(freeverb);
// const synth = new Tone.NoiseSynth().toDestination();
const duoSynth = new Tone.DuoSynth().toDestination();
// duoSynth.triggerAttackRelease("C4", "2n");

// tom.triggerAttackRelease(note, "1n");
// synth.triggerAttackRelease(0.05);

testBtn.addEventListener("click", () => {
  console.log("C6,8n");
  // synth.triggerAttackRelease(`C6`, "8n");
  duoSynth.triggerAttackRelease("C4", "2n");
});

startBtn.addEventListener("click", () => {
  // seq.start();
  console.log("C6,8n");
});

stopBtn.addEventListener("click", () => {
  // seq.stop();
});
activateBtn.addEventListener("click", () => {
  console.log('activated audio context of tonejs')
  Tone.start();
});


const minPressure = Math.min(...data.map((snapshot) => snapshot.pressure)),
  maxPressure = Math.max(...data.map((snapshot) => snapshot.pressure)),
  minCorr = Math.min(...data.map((snapshot) => snapshot.corr)),
  maxCorr = Math.max(...data.map((snapshot) => snapshot.corr));

  /**
   * @type {pitch:number,length:number,velocity:number}
   */
  // const notes = [];

const notes = data.map((snapshot) => {
  return normalizeBetweenTwoRanges(snapshot.corr, minCorr, maxCorr, 1, 127);
});

console.log(notes);


// const sequence = new Tone.Sequence(
//   (time, note) => {
//     synth.triggerAttackRelease(note, 0.1, time);
//     // subdivisions are given as subarrays
//   },
//   // I
//   notes,
//   "16n"
// ).start(0);

// var seq = new Tone.Sequence(
//   function (time, note) {
//     synth.triggerAttackRelease();
//     //straight quater notes
//   },
//   notes,
//   "16n"
// );


/**
 * Play a note
 * @param {ToneNote} note
 * @param {ToneLength} length
 */
const playNote = (note = "C4", length = "8n") => {
  synth.triggerAttackRelease(tone, length);
};
const playSequence = () => {
  Tone.Transport.start();
};
const stopSequence = () => {
  Tone.Transport.stop();
};
